from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from sorl.thumbnail import get_thumbnail
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField

from api.models import UserApp,Operator,Promotor,Catalog,Activity,Avatar,Clan,Notify,Hotel,City,Order,OrderActivity,Dispositive,Country,ValidateAccount,Package,Category,PickUp,KCoinsConfigure,Client,Money,Divisa,OrderCobrate

class UserAppSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = UserApp
        fields = ('id','status','last_login','type_user','password','user_name','related_id','is_admin')
        extra_kwargs = {'password':{'write_only':True}}

    def create(self,validate_data):
        user = super(UserAppSerializer,self).create(validate_data)

        if 'password' in validate_data:
            user.set_password(validate_data['password'])
            user.save()
        return user

    def update(self,user,validate_data):
        if 'type_user' in validate_data:
            user.type_user = validate_data['type_user']

        if 'status' in validate_data:
            user.status = validate_data['status']
            
        if 'user_name' in validate_data:
            user.user_name = validate_data['user_name']

        if 'related_id' in validate_data:
            user.related_id = validate_data['related_id']

        if 'is_admin' in validate_data:
            user.is_admin = validate_data['is_admin']

        if 'password' in validate_data:
            user.set_password(validate_data['password'])
            
        user.save()
        return user

class DispositiveSerializer(serializers.ModelSerializer):
    userapp = UserAppSerializer(many = False,read_only = True)
    userapp_id = serializers.IntegerField(required = True)
    
    class Meta:
        model = Dispositive
        fields = ('id','userapp','token','uuid','so','status','userapp_id')

class AvatarSerializer(serializers.ModelSerializer):
    url = serializers.FileField(required = False)
    class Meta:
        model = Avatar
        fields = ('id','name','status','url','is_free','kcoins')

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('__all__')

class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('__all__')

class ClanSerializer(serializers.ModelSerializer):
    poster = serializers.FileField(required = False)
    class Meta:
        model = Clan
        fields =('id','name','poster','status')

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('__all__')

class MoneySerializer(serializers.ModelSerializer):
    class Meta:
        model = Money
        fields = ('__all__')

class OperatorSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required = True)
    country_id = serializers.IntegerField(required = False)
    country = CountrySerializer(many = False,read_only = True)
    money_id = serializers.IntegerField(required = False)
    money = MoneySerializer(many = False,read_only = True)
    class Meta:
        model = Operator
        fields = ('id','name','web','phone','address','status','email_one','email_two','email_three','country','country_id','money','money_id')

class DivisaSerializer(serializers.ModelSerializer):
    money_a_value = serializers.DecimalField(max_digits=8,decimal_places=2)
    money_b_value = serializers.DecimalField(max_digits=8,decimal_places=2)
    money_a_id = serializers.IntegerField(required = True)
    money_b_id = serializers.IntegerField(required = True)
    money_a = MoneySerializer(many = False,read_only = True)
    money_b = MoneySerializer(many = False,read_only = True)
    operator = OperatorSerializer(many = False,read_only = True)
    operator_id = serializers.IntegerField(required = True)
    class Meta:
        model = Divisa
        fields = ('id','money_a','money_b','money_a_value','money_b_value','money_a_id','money_b_id','operator','operator_id','status')

class PromotorSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required = True)
    avatar_id = serializers.IntegerField(required = False)
    avatar = AvatarSerializer(many = False,read_only = True)
    clan_id = serializers.IntegerField(required = False)
    clan = ClanSerializer(many = False,read_only = True)
    operator = OperatorSerializer(many = False,read_only = True)
    operator_id = serializers.IntegerField(required = False)
    country_id = serializers.IntegerField(required = False)
    country = CountrySerializer(many = False,read_only = True)
    email = serializers.CharField(required = True,validators = [UniqueValidator(queryset=Promotor.objects.all())])

    class Meta:
        model = Promotor
        fields = ('id','name','last_name','phone','address','status','email','kcoins','avatar','avatar_id','clan','clan_id','operator','operator_id','is_freelancer','date_add','country','country_id','verify_email','verify_phone','type_promotor','lat','lng')

class ValidateAccountSerializer(serializers.ModelSerializer):
    userapp_id = serializers.IntegerField(required = False)
    userapp = UserAppSerializer(many = False,read_only = True)
    type_validate = serializers.CharField(required = False,allow_null = True)
    url_validate = serializers.CharField(required = False,allow_null = True)
    
    class Meta:
        model = ValidateAccount
        fields = ('id','code','date_add','date_expire','type_validate','url_validate','userapp_id','userapp')

class NotifySerializer(serializers.ModelSerializer):
    to_promotor = PromotorSerializer(many = False,read_only = True)
    to_promotor_id = serializers.IntegerField(required = False)

    class Meta:
        model = Notify
        fields = ('id','type_notify','message','action_id','status','to_promotor','to_promotor_id','date_add')

class PackageSerializer(serializers.ModelSerializer):
    operator = OperatorSerializer(many = False,read_only = True)
    operator_id = serializers.IntegerField(required = True)
    nro_act = serializers.IntegerField(required = True)
    dscto = serializers.IntegerField(required = True)
    status = serializers.IntegerField(required = True)

    class Meta:
        model = Package
        fields = ('id','nro_act','dscto','status','operator','date_add','operator_id')


class ActivityRelatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('__all__')

class CatalogSerializer(serializers.ModelSerializer):
    poster_100 = HyperlinkedSorlImageField(
        '500x500',
        options={"crop":"center"},
        source='poster',
        read_only=True
    )
    operator = OperatorSerializer(many = False,read_only = True)
    poster = serializers.FileField(required = False)
    operator_id = serializers.IntegerField()
    activities = ActivityRelatedSerializer(many = True,read_only = True)

    class Meta:
        model = Catalog
        fields = ('id','name','status','operator','operator_id','poster','poster_100','activities')
        #extra_kwargs = {'poster_100':{'write_only': True}}

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id','name','status')

class ActivitySerializer(serializers.ModelSerializer):
    catalog = CatalogSerializer(many = False,read_only = True)
    catalog_id = serializers.IntegerField(required = True)
    city = CitySerializer(many = False,read_only = True)
    city_id = serializers.IntegerField(required = True)
    category = CategorySerializer(many = False,read_only = True)
    category_id = serializers.IntegerField(required = True)
    poster = serializers.FileField(required = False)
    poster_500 = HyperlinkedSorlImageField(
        '500x500',
        options={"crop":"center"},
        source='poster',
        read_only=True
    )

    class Meta:
        model = Activity
        fields = ('id','name','catalog','catalog_id','status','price_pub_adult','price_pub_child',
        'price_rep_adult','price_rep_child','is_transport','city','city_id','category','category_id','poster','poster_500','max_capacity_day','description')

class PickUpSerializer(serializers.ModelSerializer):
    activity =  ActivitySerializer(many = False,read_only = True)
    activity_id = serializers.IntegerField(required = True)

    class Meta:
        model = PickUp
        fields = ('id','lu_address','ma_address','mi_address','ju_address','vi_address','sa_address','do_address','lu_time','ma_time','mi_time','ju_time','vi_time','sa_time','do_time','activity_id','activity')

class ClientSerializer(serializers.ModelSerializer):
    country = CountrySerializer(many = False,read_only = True)
    country_id = serializers.IntegerField(required = True)
    email = serializers.CharField(validators = [UniqueValidator(queryset=Client.objects.all())])
    class Meta:
        model = Client
        fields = ('id','name','email','phone','country','country_id')

class OrderSerializer(serializers.ModelSerializer):
    hotel = HotelSerializer(many = False,read_only = True)
    city = CitySerializer(many = False,read_only = True)
    operator = OperatorSerializer(many = False,read_only = True)
    hotel_id = serializers.IntegerField(required = False,allow_null = True)
    operator_id = serializers.IntegerField(required = True)
    city_id = serializers.IntegerField(required = True)
    promotor  = PromotorSerializer(many = False,read_only = True)
    promotor_id = serializers.IntegerField(required = True)
    client_id = serializers.IntegerField(required = True)
    client = ClientSerializer(many = False,read_only = True)
    money_id = serializers.IntegerField(required = True)
    money = MoneySerializer(many = False,read_only = True)
  
    class Meta:
        model = Order
        fields = ('id','city','operator','hotel','city_id','hotel_id','operator_id','phone_client','name_client','nro_hab',
        'email_client','nro_adults','nro_childs','nro_babys','total','status','promotor_id','promotor','date_order','date_order_upd','total_payment',
        'method_payment','type_payment','address_client','client_id','client','money_id','money','code_internal')

class OrderActivitySerializer(serializers.ModelSerializer):
    order = OrderSerializer(many = False,read_only = True)
    activity = ActivitySerializer(many = False,read_only = True)
    order_id = serializers.IntegerField(required = True)
    activity_id = serializers.IntegerField(required = True)
    pickup  = PickUpSerializer(many = False,read_only = True)
    pickup_id = serializers.IntegerField(required = True)

    class Meta:
        model = OrderActivity
        fields = ('id','order','activity','price_pub_adult','price_pub_child','price_rep_adult','price_rep_child','pick_up_date',
        'order_id','activity_id','pickup_id','pickup')

class KCoinsConfigureSerializer(serializers.ModelSerializer):

    class Meta:
        model = KCoinsConfigure
        fields = ("__all__")


class OrderCobrateSerializer(serializers.ModelSerializer):
    order = OrderSerializer(many = False,read_only = True)
    order_id = serializers.IntegerField(required = True)

    cobrator = PromotorSerializer(many = False,read_only = True)
    cobrator_id = serializers.IntegerField(required = True)
    class Meta:
        model = OrderCobrate
        fields = ('id','date_add','order','order_id','cobrator','cobrator_id','status')
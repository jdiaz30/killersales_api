import copy
import moment
import xlwt

from django.http import HttpResponse

from api.repository.order_repository import OrderRepository
from api.repository.userapp_repository import UserAppRepository
from api.repository.dispositive_repository import DispositiveRepository
from api.repository.notification_repository import NotificationRepository
from api.repository.client_repository import ClientRepository
from api.repository.order_cobrate_repository import OrderCobrateRepository
from api.repository.kcoins_configure_repository import KCoinsConfigureRepository

from api.serializers import OrderSerializer,DispositiveSerializer,NotifySerializer,OrderActivitySerializer,ClientSerializer,OrderCobrateSerializer,OrderActivitySerializer

from api.helpers.socket_helper import SocketHelper
from api.helpers.date_helper import DateHelper
from api.helpers.mail_helper import MailHelper

from api.services.promotor_service import PromotorService

class OrderService():
    def get_client(self,client_data):
        client_sr = ClientSerializer(data = client_data)
        if client_sr.is_valid():
            client_sr.save()
            return client_sr.data
        else:
            client = ClientRepository().get_by_email(client_data['email'])
            client_sr = ClientSerializer(client)
            return client_sr.data

    def update_activities(self,order_id,order_activities):
        for activity in order_activities:
            order_act = OrderRepository().get_activity(order_id,activity['activity_id'])
            order_act_up_sr = OrderActivitySerializer(order_act,data = activity,partial = True)
            if order_act_up_sr.is_valid():
                order_act_up_sr.save()
            else:
                print("error",order_act_up_sr.errors)

    def update_pickup_notification(self,order_id):
        order = OrderRepository().get(order_id)
        if order != None:
            order_sr = OrderSerializer(order)
            userapp = UserAppRepository().get_by_related_id("promotor",order_sr.data['promotor_id'])
            dispositives = self.get_dispositives(userapp.id)

            notify_data = {
                "type_notify":"update-pickup",
                "message":"El pickup de su reserva #"+str(order.id) +" ha sido actualizada",
                "action_id":order_sr.data['id'],
                "status":0,
                "to_promotor_id":userapp.related_id
            }

            self.saveNotify(notify_data)

            data = {
                "message": "El pickup de su reserva #"+str(order.id) +" ha sido actualizada",
                "data": dispositives
            }

            SocketHelper().emit("killer_sales:update-pickup",data)

    def update_order_status(self,order):
        userapp = UserAppRepository().get_by_related_id("promotor",order['promotor_id'])
        dispositives = self.get_dispositives(userapp.id)
        order_status_msg =  self.get_message_order_status(order['status'])
        id_order = order['id'] if order['code_internal'] == None else order['code_internal']
        message = "Su Reserva #"+str(id_order) +" ha sido " + order_status_msg
        mails_to = ["joper30@gmail.com","casf1979@gmail.com"]
        mails_to.append(order['promotor']['email'])
        mails_to.append(order['email_client'])

        notify_data = {
            "type_notify":"update-reserva",
            "message":message,
            "action_id":order['id'],
            "status":0,
            "to_promotor_id":order['promotor_id']
        }

        notify_sr = NotifySerializer(data = notify_data)

        if notify_sr.is_valid():
            notify_sr.save()

        data = {
            "message": message,
            "data": dispositives
        }

        SocketHelper().emit("killer_sales:update-order-status",data)
        order_data_email = self.prepare_data_order_email(order)

        data_email = { 
            "data": order_data_email,
            "mails_to":mails_to,
            "title":message,
            "mail_from":"reservations@behlaaktours.com"
        }

        MailHelper().send(data_email,"update-reservation")

    def get_message_order_status(self,status):
        status_list = {0:"Pendiente",1:"Confirmada",2:"Pagada",3:"Cancelada"}
        return status_list[status]

    def get_dispositives(self,userapp_id):
        dispositives = DispositiveRepository().search({"userapp_id":userapp_id})
        dispositives_sr = DispositiveSerializer(dispositives,many=True)

        return dispositives_sr.data

    def get_pickup_time(self,pickup,pickup_date):
        day = DateHelper().date_week(pickup_date)
        pickup_list = {0:pickup['do_time'],1:pickup['lu_time'],2:pickup['ma_time'],3:pickup['mi_time'],4:pickup['ju_time'],5:pickup['vi_time'],6:pickup['sa_time']}
        return pickup_list[day]

    def delete_service_order(self,order_id,services):
        for service in services:
            OrderRepository().delete_activity(order_id,service)

    def prepare_data_order_email(self,order):
        order_activities = OrderRepository().get_activities(order['id'])
        order_act_sr = OrderActivitySerializer(order_activities,many = True)
        total_childs_rep = 0
        total_adults_rep = 0

        total_childs_pub = 0
        total_adults_pub = 0

        data = {}

        order_activities_cp = copy.deepcopy(order_act_sr.data)
        for activity in order_activities_cp:
            activity['pickup_text'] = DateHelper().date_format(activity['pick_up_date'],'YYYY-MM-DD')
            activity['pickup_time'] = self.get_pickup_time(activity['pickup'],activity['pickup_text'])

            total_adults_rep += float(activity['price_rep_adult']) if order['nro_adults'] > 0 else 0
            total_childs_rep += float(activity['price_rep_child']) if order['nro_childs'] > 0 else 0

            total_adults_pub += float(activity['price_pub_adult']) if order['nro_adults'] > 0 else 0
            total_childs_pub += float(activity['price_pub_child']) if order['nro_childs'] > 0 else 0

        total_adults_rep =  total_adults_rep  if order['nro_adults'] == 0 else total_adults_rep * int(order['nro_adults'])
        total_childs_rep =  total_childs_rep  if order['nro_childs'] == 0 else total_childs_rep * int(order['nro_childs'])

        total_adults_pub =  total_adults_pub  if order['nro_adults'] == 0 else total_adults_pub * int(order['nro_adults'])
        total_childs_pub =  total_childs_pub  if order['nro_childs'] == 0 else total_childs_pub * int(order['nro_childs'])

        order_data = copy.deepcopy(order)
        order_data['date_order_text']= DateHelper().date_format(order['date_order'],'YYYY-MM-DD')

        data["order"] = order_data
        data["total_adults_rep"] = total_adults_rep
        data["total_childs_rep"] = total_childs_rep

        data["total_adults_pub"] = total_adults_pub
        data["total_childs_pub"] = total_childs_pub

        data["activities"] = order_activities_cp

        data["total_public"] = total_adults_pub + total_childs_pub
        data["balance"] =  round(float(data["total_public"]) - float(order['total_payment']),2)

        return data

    def generate_order_cobrate(self,order,cobrator_id):
        response = {"success": False, "data" : ""}
        if order.status == 1:
            order_cobrate = { "order_id" : order.id,"cobrator_id" : cobrator_id,"status" : 0}
            order_cobrate_sr = OrderCobrateSerializer(data = order_cobrate)
            exists_order_cobrate = OrderCobrateRepository().exists_cobrate(cobrator_id,order.id)

            notify_data = {
                "type_notify":"create-order-cobrate",
                "message":"Tiene una orden de cobranza de su reserva #"+str(order.id),
                "action_id":order.id,
                "status":0,
                "to_promotor_id":order.promotor_id
            }

            self.saveNotify(notify_data)

            if exists_order_cobrate == None:
                if order_cobrate_sr.is_valid():
                    order_cobrate_sr.save()
                    
            response['success'] = True
            push_data = {"to_promotor_id":"killer-"+str(order.promotor_id),"message": notify_data['message']}
            SocketHelper().emit('killer_sales:order-cobrate',push_data)
        
        return response

    def get_total_kcoins(self,nro_childs,nro_adults):
        total_childs = 0
        total_adults = 0
        total_kcoins = 0

        if nro_childs > 0:
            kcoins_childs = KCoinsConfigureRepository().get_kcoins_type_pax("children")
            total_childs =  (int(nro_childs / kcoins_childs.pax) * kcoins_childs.kcoins) if kcoins_childs != None else 0
        
        if nro_adults > 0:
            kcoins_adults = KCoinsConfigureRepository().get_kcoins_type_pax("adults")
            total_adults = (int(nro_adults / kcoins_adults.pax) * kcoins_adults.kcoins) if kcoins_adults != None else 0
            
        total_kcoins = total_adults + total_childs

        return total_kcoins


    def saveNotify(self,notify):
        notify_sr = NotifySerializer(data = notify)

        if notify_sr.is_valid():
            notify_sr.save()


    def calculate_total_report(self,activities,nro_adults,nro_childs):
        total_adults_re = 0
        total_childs_re = 0
        total_adults_pub = 0
        total_childs_pub = 0
        total_report = {"report":0,"public":0}

        for activity in activities:
            if nro_adults > 0:
                total_adults_re += float(activity['price_rep_adult']) * nro_adults
                total_adults_pub += float(activity['price_pub_adult']) * nro_adults
            if nro_childs > 0:
                total_childs_re += float(activity['price_rep_child']) * nro_childs
                total_childs_pub += float(activity['price_pub_child']) * nro_childs

        total_report["public"] = total_adults_pub + total_childs_pub
        total_report["report"] = total_adults_re + total_childs_re

        return total_report

    def get_text_status(self,status):
        status_text = ["Pendiente","Reservado","Pagado","Cancelado"]
        return status_text[status]

    def report_by_asesor_original(self,params):
        data = OrderRepository().search(params)
        order = OrderSerializer(data,many = True)
        #return order.data

        response = HttpResponse(content_type='text/ms-excel')
        response['Content-Disposition'] = 'attachment;filename="data.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Data')
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        font_style_text = xlwt.XFStyle()
        font_style_text.font.bold = False

        columns = ['CODIGO','OPERADOR','NRO ADULTOS','NRO NIÑOS','MONEDA','T/C','DEPOSITO','BALANCE','COMISION','PRE. REPORTE TOTAL','PRE. TOTAL BL','STATUS','PICK UP','FECHA','ASESOR','CLIENTE','TELEFONO CLIENTE','EMAIL CLIENTE','CLIENTE DIRECCION','CLIENTE NRO HAB.','HOTEL','METODO DE PAGO','TIPO DE PAGO','CANT. ACTIVIDADES','ACTIVIDADES']

        for col_num in range(len(columns)):
            ws.write(row_num,col_num,columns[col_num],font_style)
        
        for i, order_item in enumerate(order.data):
            order_act = OrderRepository().get_activities(order_item['id'])
            order_act_sr = OrderActivitySerializer(order_act,many = True)
            total_report = self.calculate_total_report(order_act_sr.data,order_item['nro_adults'],order_item['nro_childs'])
            text_status = self.get_text_status(order_item['status'])
            comision = total_report['public'] - total_report['report']
            balance = 0

            if order_item['type_payment'] == "deposito":
                balance = float(order_item["total"]) - float(order_item["total_payment"])

            ws.write(i+1,0,order_item['code_internal'],font_style_text)
            ws.write(i+1,1,order_item['operator']['name'],font_style_text)
            ws.write(i+1,2,order_item['nro_adults'],font_style_text)
            ws.write(i+1,3,order_item['nro_childs'],font_style_text)
            ws.write(i+1,4,order_item['operator']['money']['name'],font_style_text)
            ws.write(i+1,5,"0",font_style_text)

            ws.write(i+1,6,order_item['total_payment'],font_style_text)
            ws.write(i+1,7,balance,font_style_text)
            ws.write(i+1,8,comision,font_style_text)
            ws.write(i+1,9,total_report["report"],font_style_text)
            ws.write(i+1,10,total_report["public"],font_style_text)

            ws.write(i+1,11,text_status,font_style_text)

            pickup = ""
            activities = ""
            hotel = order_item['hotel']['name'] if order_item['hotel'] != None else ""

            if len(order_act_sr.data) > 0 :
                pickup =  order_act_sr.data[0]['pickup']['lu_address'] if 'lu_address' in order_act_sr.data[0]['pickup'] else ""
                for act_item in order_act_sr.data:
                    activities += act_item['activity']['name'] + " / "
 
            ws.write(i+1,12,pickup,font_style_text)
            ws.write(i+1,13,order_item['date_order'],font_style_text)
            ws.write(i+1,14,order_item['promotor']['name'] +" "+ order_item['promotor']['last_name'],font_style_text)
            ws.write(i+1,15,order_item['name_client'],font_style_text)
            ws.write(i+1,16,order_item['phone_client'],font_style_text)
            ws.write(i+1,17,order_item['email_client'],font_style_text)
            ws.write(i+1,18,order_item['address_client'],font_style_text)
            ws.write(i+1,19,order_item['nro_hab'],font_style_text)
            ws.write(i+1,20,hotel,font_style_text)
            ws.write(i+1,21,order_item['method_payment'],font_style_text)
            ws.write(i+1,22,order_item['type_payment'],font_style_text)
            ws.write(i+1,23,len(order_act_sr.data),font_style_text)
            ws.write(i+1,24,activities,font_style_text)

        wb.save(response)

        return response

    def report_by_asesor(self,params):
        data = OrderRepository().search(params)
        order = OrderSerializer(data,many = True)
        #return order.data

        response = HttpResponse(content_type='text/ms-excel')
        response['Content-Disposition'] = 'attachment;filename="data.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Data')
        
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        font_style_text = xlwt.XFStyle()
        font_style_text.font.bold = False

        columns = ['CODIGO','OPERADOR','NRO ADULTOS','NRO NIÑOS','MONEDA','T/C','DEPOSITO','BALANCE','COMISION','PRE.REP ADULT','PRE.VENTA ADULT','PRE.REP NIÑO','PRE.VENTA NIÑO','SUBTOTAL REP.ADULT','SUBTOTAL VENTA ADULT','SUBTOTAL REP.NIÑO','SUBTOTAL VENTA.NIÑO','TOTAL REPORTE','TOTAL VENTA', 'STATUS','PICK UP','FECHA','ASESOR','CLIENTE','TELEFONO CLIENTE','EMAIL CLIENTE','CLIENTE DIRECCION','CLIENTE NRO HAB.','HOTEL','METODO DE PAGO','TIPO DE PAGO','CANT. ACTIVIDADES','ACTIVIDADES']

        for col_num in range(len(columns)):
            ws.write(row_num,col_num,columns[col_num],font_style)
        
        for j, order_item in enumerate(order.data):
            order_act = OrderRepository().get_activities(order_item['id'])
            order_act_sr = OrderActivitySerializer(order_act,many = True)
            total_report = self.calculate_total_report(order_act_sr.data,order_item['nro_adults'],order_item['nro_childs'])
            text_status = self.get_text_status(order_item['status'])
            comision = total_report['public'] - total_report['report']
            balance = 0
            total_activities = len(order_act_sr.data)
            total_reporf = 0
            total_publicf = 0

            if order_item['type_payment'] == "deposito":
                balance = float(order_item["total"]) - float(order_item["total_payment"])

            pickup = ""
            activities = ""
            hotel = order_item['hotel']['name'] if order_item['hotel'] != None else ""

            if len(order_act_sr.data) > 0 :
                pickup =  order_act_sr.data[0]['pickup']['lu_address'] if 'lu_address' in order_act_sr.data[0]['pickup'] else ""

            if len(order_act_sr.data) >= 2:
                row_num += 1
                ws.write(row_num,0,"",font_style_text)

            for i, act_item in enumerate(order_act_sr.data):
                    #activities += act_item['activity']['name'] + " / "
                row_num += 1
                ws.write(row_num,0,order_item['code_internal'],font_style_text)
                ws.write(row_num,1,order_item['operator']['name'],font_style_text)
                ws.write(row_num,2,order_item['nro_adults'],font_style_text)
                ws.write(row_num,3,order_item['nro_childs'],font_style_text)
                ws.write(row_num,4,order_item['operator']['money']['name'],font_style_text)
                ws.write(row_num,5,"0",font_style_text)

                total_payment = float(order_item['total_payment']) / total_activities

                ws.write(row_num,6,total_payment,font_style_text)
                ws.write(row_num,7,balance / total_activities,font_style_text)
                ws.write(row_num,8,comision,font_style_text)
                #ws.write(row_num,9,total_report["report"],font_style_text)
                #ws.write(row_num,10,total_report["public"],font_style_text)

                ws.write(row_num,9,act_item["price_rep_adult"],font_style_text)
                ws.write(row_num,10,act_item["price_pub_adult"],font_style_text)
                ws.write(row_num,11,act_item["price_rep_child"],font_style_text)
                ws.write(row_num,12,act_item["price_pub_child"],font_style_text)

                subt_price_rep_adult = int(order_item['nro_adults']) * float(act_item["price_rep_adult"])
                subt_price_pub_adult = int(order_item['nro_adults']) * float(act_item["price_pub_adult"])

                subt_price_rep_child = int(order_item['nro_childs']) * float(act_item["price_rep_child"])
                subt_price_pub_child = int(order_item['nro_childs']) * float(act_item["price_pub_child"])


                ws.write(row_num,13,subt_price_rep_adult,font_style_text)
                ws.write(row_num,14,subt_price_pub_adult,font_style_text)
                ws.write(row_num,15,subt_price_rep_child,font_style_text)
                ws.write(row_num,16,subt_price_pub_child,font_style_text)

                total_report = subt_price_rep_adult + subt_price_rep_child
                total_public = subt_price_pub_adult + subt_price_pub_child

                total_reporf +=  total_report
                total_publicf += total_public

                ws.write(row_num,17,total_report,font_style_text)
                ws.write(row_num,18,total_public,font_style_text)

                ws.write(row_num,19,text_status,font_style_text)
                
                ws.write(row_num,20,pickup,font_style_text)
                ws.write(row_num,21,order_item['date_order'],font_style_text)
                ws.write(row_num,22,order_item['promotor']['name'] +" "+ order_item['promotor']['last_name'],font_style_text)
                ws.write(row_num,23,order_item['name_client'],font_style_text)
                ws.write(row_num,24,order_item['phone_client'],font_style_text)
                ws.write(row_num,25,order_item['email_client'],font_style_text)
                ws.write(row_num,26,order_item['address_client'],font_style_text)
                ws.write(row_num,27,order_item['nro_hab'],font_style_text)
                ws.write(row_num,28,hotel,font_style_text)
                ws.write(row_num,29,order_item['method_payment'],font_style_text)
                ws.write(row_num,30,order_item['type_payment'],font_style_text)
                ws.write(row_num,31,len(order_act_sr.data),font_style_text)
                ws.write(row_num,32,act_item['activity']['name'],font_style_text)

            if len(order_act_sr.data) >= 2:
                row_num += 1
                ws.write(row_num,6,order_item['total_payment'],font_style_text)
                ws.write(row_num,7,balance,font_style_text)
                ws.write(row_num,17,total_reporf,font_style_text)
                ws.write(row_num,18,total_publicf,font_style_text)

                row_num += 1
                ws.write(row_num,0,"",font_style_text)


        wb.save(response)

        return response
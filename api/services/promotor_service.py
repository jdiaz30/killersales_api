import copy
import moment

from api.repository.promotor_repository import PromotorRepository
from api.repository.order_repository import OrderRepository

from api.serializers import PromotorSerializer,NotifySerializer

from api.helpers.socket_helper import SocketHelper
from api.helpers.date_helper import DateHelper 
from api.helpers.mail_helper import MailHelper

class PromotorService():
    def get_stats(self,promotor_id):
        try:
            stats = {"deals":0,"top":0,"kcoins":0}
            deals = OrderRepository().search_total({"status":2,"promotor_id":promotor_id})
            promotor = PromotorRepository().get(promotor_id)

            stats['deals'] = deals
            stats['kcoins'] = promotor.kcoins

            return stats
        except:
            return False

    def add_kcoins(self,promotor_id,kcoins,order_id):
        promotor = PromotorRepository().get(promotor_id)
        kcoins_upd = promotor.kcoins + kcoins
        promotor_sr = PromotorSerializer(promotor,data = {"kcoins":kcoins_upd},partial = True)

        if promotor_sr.is_valid():
            promotor_sr.save()

            notify_data = {
                "type_notify":"add-kcoins",
                "message":"Haz ganado "+str(kcoins)+" kcoins por tu reserva #"+str(order_id),
                "action_id":order_id,
                "status":0,
                "to_promotor_id":promotor_id
            }

            self.saveNotify(notify_data)

            push_data = {"to_promotor_id":"killer-"+str(promotor_id),"message": notify_data['message']}
            SocketHelper().emit('killer_sales:add-kcoins',push_data)

    #Cobranza aceptada
    def notify_cobrator_success(self,promotor_id,order_id,order_cobrate_id):
        notify_data = {
                "type_notify":"cobrate-success",
                "message":"Su orden de cobranza #"+str(order_cobrate_id)+" para la reserva #"+str(order_id) +" ha sido confirmada y pagada",
                "action_id":order_id,
                "status":0,
                "to_promotor_id":promotor_id
        }

        self.saveNotify(notify_data)

        push_data = {"to_promotor_id":"killer-"+str(promotor_id),"message": notify_data['message']}
        SocketHelper().emit('killer_sales:add-kcoins',push_data)

    def saveNotify(self,notify):
        notify_sr = NotifySerializer(data = notify)

        if notify_sr.is_valid():
            notify_sr.save()
        
        
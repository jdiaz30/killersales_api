from api.models import Country

class CountryRepository():
    def search(self,query):
        filters = {}

        if "name" in query:
            filters['name__icontains'] = query['name']
        if "status" in query:
            filters['status'] = query['status']
   
        return Country.objects.filter(** filters)     
    
    def get(self,id):
        try:
            return Country.objects.get(pk = id)
        except Country.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        country = self.get(id)
        
        if country != None:
            country.delete()
            response = True
        
        return response
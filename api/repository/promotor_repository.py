
from api.models import Promotor
from api.helpers.socket_helper import SocketHelper
from api.repository.userapp_repository import UserAppRepository
from api.repository.dispositive_repository import DispositiveRepository
from api.serializers import UserAppSerializer,DispositiveSerializer

class PromotorRepository():
    def search(self,query):
        filters = {}
        exclude_id = 0
        for params in query:
            if params == "clan_id":
                filters['clan_id'] = query['clan_id']
            if params == "name":
                filters['name__icontains'] = query['name']
                #filters['last_name__icontains'] = query['name']
            if params == "exclude_id":
                exclude_id = query['exclude_id']
            if params == "operator_id":
                filters['operator_id'] = query['operator_id']
            if params == "type_promotor":
                filters['type_promotor'] = query['type_promotor']

        return Promotor.objects.filter(** filters).exclude(id=exclude_id).order_by('-id')

    def get_by_email(self,email):
        try:
            return Promotor.objects.get(email = email)
        except Promotor.DoesNotExist:
            return None

    def get(self,id):
        try:
            return Promotor.objects.get(pk = id)
        except Promotor.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        promotor = self.get(id)
        
        if promotor != None:
            promotor.delete()
            response = True
        
        return response

    
    def total_promotors_by_operator(self,query):
        filters = {}

        if 'operator_id' in query:
            filters['operator_id'] = query['operator_id']
        if 'date_add' in query:
                filters['date_add__startswith'] = query['date_add']

        return Promotor.objects.filter(** filters).count()

    def activate_account(self,promotor,activate):
        userapp = UserAppRepository().get_by_user_name(promotor['email'])
        if userapp !=None:
            userapp_data = {"status": activate}
            userapp_sr = UserAppSerializer(userapp,data=userapp_data,partial = True)

            if userapp_sr.is_valid():
                userapp_sr.save()
            
            dispositives = DispositiveRepository().search({"userapp_id":userapp_sr.data['id']})
            dispositives_sr = DispositiveSerializer(dispositives,many=True)

            data = {
                "message": "Hemos desactivado su cuenta" if activate == "0" else "Hemos activado su cuenta",
                "data": dispositives_sr.data
            }

            SocketHelper().emit("killer_sales:activate-promotor",data)
        
    def validate_phone(self,userapp_id):
        userapp = UserAppRepository().get(userapp_id)
        promotor = self.get(userapp.related_id)

        if promotor != None:
            promotor.verify_phone = True
            promotor.verify_email = True
            promotor.save()
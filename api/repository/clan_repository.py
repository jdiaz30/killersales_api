from api.models import Clan

class ClanRepository():
    def search(self):
        return Clan.objects.all()     
    
    def get(self,id):
        try:
            return Clan.objects.get(pk = id)
        except Clan.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        clan = self.get(id)
        
        if clan != None:
            clan.delete()
            response = True
        
        return response
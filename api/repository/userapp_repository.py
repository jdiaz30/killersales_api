from django.contrib.auth.hashers import check_password
from rest_framework_jwt.settings import api_settings
from django.db.models import Q

from api.models import UserApp,Promotor
from api.serializers import PromotorSerializer,UserAppSerializer
from api.helpers.jwt_customize_helper import JwtCustomizeHelper

class UserAppRepository():

    def search(self,query):
        filters = {}
        exclude_id = 0
        
        if 'exclude_id' in query:
            exclude_id = query['exclude_id']
        if 'name' in query:
            filters['user_name__icontains'] = query['user_name']
    
        return UserApp.objects.filter(** filters).exclude(id=exclude_id).order_by('-id')

    def get_by_user_name(self,user_name):
        try:
            return UserApp.objects.get(user_name = user_name)
        except UserApp.DoesNotExist:
            return None

    def get_by_related_id(self,type_user,related_id):
        try:
            return UserApp.objects.get(type_user = type_user,related_id = related_id,status = 1)
        except UserApp.DoesNotExist:
            return None

    def get(self,id):
        try:
            return UserApp.objects.get(pk = id)
        except UserApp.DoesNotExist:
            return None

    def auth(self,query,query_params):
        response ={ "data": None ,"message":"","message_code":0}
        filters = {}
        user = {}
        try:
            filters['user_name'] = query['user_name']

            if 'source' in query_params:
                if query_params['source'] == "app":
                    user = UserApp.objects.get(Q(type_user__icontains = 'promotor') | Q(type_user__icontains = 'cobrator'),** filters )
                else:
                    #filters['type_user__icontains'] = "admin"
                    user = UserApp.objects.get(Q(type_user__icontains = 'admin') | Q(type_user__icontains = 'cobrator') | Q(type_user__icontains = 'operator')| Q(is_admin = True),** filters)
            if check_password(query['password'],user.password):
                if user.status == 0:
                    promotor = Promotor.objects.get(pk = user.related_id)
                    if promotor.verify_phone == False:
                        promotor_sr = PromotorSerializer(promotor)
                        userapp_sr = UserAppSerializer(user)

                        response['message'] = 'Falta activar su cuenta y verificar su telefono'
                        response['messsage_code'] = 2
                        response['data'] = {
                            "promotor": promotor_sr.data,
                            "userapp": userapp_sr.data,
                            "message_code": 2
                        }
                    else:
                        response['message'] = "Su cuenta no esta activada"    
                else:
                    user_data = self.get_by_user_name(query['user_name'])
                    response['message'] = "Credenciales son correctas"
                    response['message_code'] = 1
                    response['data'] = user
                    response['promotor'] = user_data
            else:
                response['message'] = "Contraseña no es válida"
                response['data'] = None
                response['message_code'] = 3
        except UserApp.DoesNotExist:
            response['message'] = "Credenciales no son correctas"
            response['data'] = None
        
        return response

    def create_token_social(self,data):
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = JwtCustomizeHelper().jwt_payload_handler(data)
        token = jwt_encode_handler(payload)

        return {"token":token}

    def delete(self,id):
        response = False
        user = self.get(id)
        
        if user != None:
            user.delete()
            response = True
        
        return response


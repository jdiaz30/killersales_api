from api.models import Category

class CategoryRepository():
    def search(self,query):
        filters = {}
   
        if "status" in query:
            filters['status'] = query['status']    
    
        return Category.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return Category.objects.get(pk = id)
        except Category.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        category = self.get(id)
        
        if category != None:
            category.delete()
            response = True
        
        return response   

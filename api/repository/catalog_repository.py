from api.models import Catalog

class CatalogRepository():
    def search(self,query):
        filters = {}
        if 'operator_id' in query:
            filters['operator_id'] = query['operator_id']
        if 'status' in query:
            filters['status'] = query['status']
        if 'name' in query:
            filters['name__icontains'] = query['name']

        return Catalog.objects.filter(** filters)     
    
    def get(self,id):
        try:
            return Catalog.objects.get(pk = id)
        except Catalog.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        catalog = self.get(id)
        
        if catalog != None:
            catalog.delete()
            response = True
        
        return response
       

from api.models import Divisa

class DivisaRepository():
    def search(self,query):
        filters = {}
    
        if 'operator_id' in query:
            filters['operator_id'] = query['operator_id']
        if 'money_a_id' in query:
            filters['money_a_id'] = query['money_a_id']
        if 'money_b_id' in query:
            filters['money_b_id'] = query['money_b_id']
        if 'status' in query:
            filters['status'] = query['status']
    
        return Divisa.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return Divisa.objects.get(pk = id)
        except Divisa.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        divisa = self.get(id)
        
        if divisa != None:
            divisa.delete()
            response = True
        
        return response   

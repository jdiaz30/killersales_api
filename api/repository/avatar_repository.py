from api.models import Avatar

class AvatarRepository():
    def search(self,query):
        filters = {}

        if 'status' in query:
            filters['status'] = query['status']

        if 'kcoins' in query:
            filters['kcoins'] = query['kcoins']

        if 'name' in query:
            filters['name__icontains'] = query['name']

        return Avatar.objects.filter( ** filters)
    
    def get(self,id):
        try:
            return Avatar.objects.get(pk = id)
        except Avatar.DoesNotExist:
            return None
    
    def delete(self,id):
        response = False
        avatar = self.get(id)
        
        if avatar != None:
            avatar.delete()
            response = True
        
        return response
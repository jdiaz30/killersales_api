from api.models import Package

class PackageRepository():
    def search(self,query):
        filters = {}
     
        if "operator_id" in query:
            filters['operator_id'] = query['operator_id']

        return Package.objects.filter(** filters) 
    
    def get(self,id):
        try:
            return Package.objects.get(pk = id)
        except Package.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        package = self.get(id)
        
        if package != None:
            package.delete()
            response = True
        
        return response
from api.models import Money

class MoneyRepository():
    def search(self,query):
        filters = {}
     
        if "name" in query:
            filters['name__icontains'] = query['name']
        if "status" in query:
            filters['status'] = query['status']

        return Money.objects.filter(** filters)

    def get(self,id):
        try:
            return Money.objects.get(pk = id)
        except Money.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        money = self.get(id)
        
        if money != None:
            money.delete()
            response = True
        
        return response
        
       

from api.models import Client

class ClientRepository():
    def search(self,query):
        filters = {}
    
        if 'name' in query:
            filters['name__icontains'] = query['name']
    
        return Client.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return Client.objects.get(pk = id)
        except Client.DoesNotExist:
            return None

    def get_by_email(self,email):
        try:
            return Client.objects.get(email = email)
        except Client.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        client = self.get(id)
        
        if client != None:
            client.delete()
            response = True
        
        return response   

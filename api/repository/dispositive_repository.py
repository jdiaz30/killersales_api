
from api.models import Dispositive

class DispositiveRepository():

    def get(self,id):
        try:
            return Dispositive.objects.get(pk = id)
        except Dispositive.DoesNotExist:
            return None

    def search(self,query):
        filters = {}
        
        if "userapp_id" in query:
            filters['userapp_id'] = query['userapp_id']
    
        return Dispositive.objects.filter(** filters)

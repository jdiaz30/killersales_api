from api.models import Notify

class NotificationRepository():
    def search(self,query):
        filters = {}
        for params in query:
            if params == "type_notify":
                filters['type_notify'] = query['type_notify']
            if params == "to_promotor_id":
                filters['to_promotor_id'] = query['to_promotor_id']
            if params == "status":
                filters['status'] = query['status']    
    
        return Notify.objects.filter(** filters).order_by('-date_add')  
    
    def get(self,id):
        try:
            return Notify.objects.get(pk = id)
        except Notify.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        notify = self.get(id)
        
        if notify != None:
            notify.delete()
            response = True
        
        return response
       

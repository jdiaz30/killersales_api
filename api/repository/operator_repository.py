from api.models import Operator

class OperatorRepository():
    def search(self,query):
        filters = {}
        exclude_id = 0
        
        if "name" in query:
            filters['name__icontains'] = query['name']
        if "operator_id" in query:
            filters['id'] = query['operator_id']

        return Operator.objects.filter(** filters)

    def get(self,id):
        try:
            return Operator.objects.get(pk = id)
        except Operator.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        operator = self.get(id)
        
        if operator != None:
            operator.delete()
            response = True
        
        return response
        
       

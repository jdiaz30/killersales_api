from api.models import Hotel

class HotelRepository():
    def search(self,query):
        filters = {}
        for params in query:
            if params == "status":
                filters['status'] = query['status']
            if params == "name":
                filters['name__icontains'] = query['name']   
    
        return Hotel.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return Hotel.objects.get(pk = id)
        except Hotel.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        hotel = self.get(id)
        
        if hotel != None:
            hotel.delete()
            response = True
        
        return response   

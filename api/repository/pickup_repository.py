from api.models import PickUp

class PickUpRepository():
    def search(self,query):
        filters = {}
        if 'activity_id' in query:
            filters['activity_id'] = query['activity_id']
        
        return PickUp.objects.filter(** filters)

    def get_all_by_activity(self,activity_id):     
        return PickUp.objects.filter(activity_id = activity_id)
    
    def get(self,id):
        try:
            return PickUp.objects.get(pk = id)
        except PickUp.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        pickup = self.get(id)
        
        if pickup != None:
            pickup.delete()
            response = True
        
        return response
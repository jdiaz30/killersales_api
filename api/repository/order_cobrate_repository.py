from api.models import OrderCobrate

class OrderCobrateRepository():
    def search(self,query):
        filters = {}
        if  "cobrator_id" in query:
            filters['cobrator_id'] = query['cobrator_id']
        return OrderCobrate.objects.filter(** filters)
    
    def exists_cobrate(self,cobrator_id,order_id):
        try:
            return OrderCobrate.objects.get(order_id = order_id,cobrator_id= cobrator_id)
        except OrderCobrate.DoesNotExist:
            return None
    
    def get(self,id):
        try:
            return OrderCobrate.objects.get(pk = id)
        except OrderCobrate.DoesNotExist:
            return None
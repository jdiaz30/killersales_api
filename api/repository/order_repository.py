from api.models import Order,OrderActivity,Activity,OrderCobrate
from django.db.models import Sum
from datetime import datetime
import pytz
from geopy import distance

class OrderRepository():

    def search_total(self,query):
        filters = self.prepare_filters(query)
        return Order.objects.filter(**filters).count()

    def search_order_cobrate(self,order_id):
        return OrderCobrate.objects.get(order_id = order_id)
    
    def get_order_cobrate(self,id):
        try:
            return OrderCobrate.objects.get(id = id)
        except OrderCobrate.DoesNotExist:
            return None

    def search_geo(self,query):
        filters = {}
        lat = query['lat']
        lng = query['lng']
        d = float(query['distance'])
        
        if  "status" in query:
            filters['status'] = query['status']

        filters['promotor__lat__gt'] = float(lat) - 1
        filters['promotor__lat__lt'] = float(lat) + 1 
        filters['promotor__lng__gt'] = float(lng) - 1
        filters['promotor__lng__lt'] = float(lng) + 1
          
        orders = Order.objects.filter(**filters).order_by('-date_order')
        orders_distance = []
        
        for order in orders:
            d_calculate = distance.distance((lat,lng),(order.promotor.lat,order.promotor.lng)).km
            if d_calculate <= d:
                orders_distance.append(order)
           
        return orders_distance

    def get_last_id(self):
        return Order.objects.latest('id').id

    def prepare_filters(self,query):
        filters = {}
        
        if  "status" in query:
            filters['status'] = query['status']
        if "date_order" in query:
            #filters['date_order__range'] = [query['date_order'],'2018-10-11']
            filters['date_order__startswith'] = query['date_order']
            #filters['date_order__contains'] = query['date_order']

        if  "date_order_ini" in query:
            filters['date_order__range'] = [query['date_order_ini'],query['date_order_end']]

        if  "operator_id" in query:
            filters['operator_id'] = query['operator_id']

        if  "promotor_id" in query:
            if query['promotor_id'] != "0":
                filters['promotor_id'] = query['promotor_id']

        if "operator_id" in query:
            filters['promotor__operator_id'] = query['operator_id']
                

        return filters


    def search(self,query):
        filters = self.prepare_filters(query)
        return Order.objects.filter(**filters).order_by('-date_order')

    def get_promotor(self,promotor_id):
        return Order.objects.filter(promotor_id = promotor_id).order_by('-date_order')
    
    def get(self,id):
        try:
            return Order.objects.get(pk = id)
        except Order.DoesNotExist:
            return None

    def get_activities(self,order_id):
        return OrderActivity.objects.filter(order_id = order_id)

    def get_activity(self,order_id,activity_id):
        try:
            return OrderActivity.objects.get(order_id = order_id,activity_id = activity_id)
        except OrderActivity.DoesNotExist:
            return None

    def get_stats(self,query):
        filters = {}
        data = {
            "total_pendients":0,
            "total_reserveds":0,
            "total_canceleds":0,
            "total_factured":0,
            "total_payments":0
        }

        for params in query:
            if params == "operator_id":
                filters['operator_id'] = query['operator_id']
            if params == "date_order":
                #filters['date_order__range'] = [query['date_order'],'2018-10-11']
                filters['date_order__startswith'] = query['date_order']

            if params == "date_order_ini":
                filters['date_order__range'] = [query['date_order_ini'],query['date_order_end']]

        filters.update({"status":0})
        data['total_pendients'] = Order.objects.filter(**filters).count()

        filters["status"] = 1
        data['total_reserveds'] = Order.objects.filter(**filters).count()
        
        filters["status"] = 2
        data['total_factured'] = Order.objects.filter(**filters).aggregate(Sum('total'))['total__sum']
        data['total_factured'] = 0 if data['total_factured'] == None else data['total_factured']

        filters["status"] = 3
        data['total_canceleds'] = Order.objects.filter(**filters).count()

        filters["status"] = 2
        data['total_payments'] = Order.objects.filter(**filters).count()

        return data

    def delete(self,id):
        response = False
        order = self.get(id)
        
        if order != None:
            order.delete()
            response = True
        
        return response

    def delete_activity(self,order_id,activity_id):
        activity = self.get_activity(order_id,activity_id)
        if activity !=None:
            activity.delete()

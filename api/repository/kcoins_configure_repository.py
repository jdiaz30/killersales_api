from api.models import KCoinsConfigure

class KCoinsConfigureRepository():
    def search(self,query):
        filters = {}
        for params in query:
            if params == "pax":
                filters['pax'] = query['pax']
            if params == "type_pax":
                filters['type_pax'] = query['type_pax']   
    
        return KCoinsConfigure.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return KCoinsConfigure.objects.get(pk = id)
        except KCoinsConfigure.DoesNotExist:
            return None

    def get_kcoins_type_pax(self,type_pax):
        try:
            return KCoinsConfigure.objects.get(type_pax = type_pax)
        except KCoinsConfigure.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        kcoins = self.get(id)
        
        if kcoins != None:
            kcoins.delete()
            response = True
        
        return response   

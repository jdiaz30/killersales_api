from api.models import Activity,OrderActivity

class ActivityRepository():
    def get(self,id):
        try:
            return Activity.objects.get(pk = id)
        except Activity.DoesNotExist:
            return None

    def search(self,query):
        filters = {}
        if 'status' in query:
            filters['status'] = query['status']

        return Activity.objects.filter(** filters)

    def get_all_by_catalog(self,catalog_id):
        return Activity.objects.filter(catalog_id = catalog_id,status=1)

    def delete(self,id):
        orders = OrderActivity.objects.filter(activity_id = id).count()
        activity = self.get(id)
       
        if orders >0:
            activity.status = 2
            activity.save()
        else:
            activity.delete()

    def get_available_by_date(self,activity_id,date):
        activity = self.get(activity_id)
        response = {"max_capacity_day":0,"total_orders":0}

        if activity != None:
            response['max_capacity_day'] = activity.max_capacity_day
            orders_activity = OrderActivity.objects.filter(pick_up_date__startswith = date,order__status = 1).count()
            response['total_orders'] = orders_activity
            
        return response
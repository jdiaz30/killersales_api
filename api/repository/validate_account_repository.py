import random
from datetime import datetime,timedelta
import moment

from api.models import ValidateAccount
from api.repository.userapp_repository import UserAppRepository
from api.serializers import ValidateAccountSerializer
from api.helpers.socket_helper import SocketHelper
from api.helpers.date_helper import DateHelper

class ValidateAccountRepository():
    def exists_code(self,query):
        filters = {}
        date = datetime.strptime(query['date_add'],'%Y-%m-%d %H:%M:%S')
        response = { "message_code": 0}

        try:
            if 'userapp' in query:
                filters['userapp_id'] = query['userapp']
            if 'code' in query:
                filters['code'] = query['code']

            data = ValidateAccount.objects.get(** filters)
    
            date_add_text = datetime.strftime(data.date_add,'%Y-%m-%d %H:%M:%S')
            date_add =  datetime.strptime(date_add_text,'%Y-%m-%d %H:%M:%S') + timedelta(minutes = 5)

            response['data'] = None

            if  date <= date_add:
                response['message'] = "Codigo es valido"
                response['message_code'] = 2
                response['data'] = data
            else:
                response['message'] = "Codigo ha expirado"
                response['message_code'] = 1

            return response

        except ValidateAccount.DoesNotExist:
            return response

    def exists_url(self,url):
        response = { "message_code" : 0, "data":None }
        try:
            data = ValidateAccount.objects.filter(url_validate = url).order_by('-date_expire')
            date = datetime.utcnow()

            if  len(data) > 0:

                date_add_text = datetime.strftime(data[0].date_add,'%Y-%m-%d %H:%M:%S')
                date_add =  datetime.strptime(date_add_text,'%Y-%m-%d %H:%M:%S') + timedelta(minutes = 5)

                if date <= date_add:
                    response['message_code'] = 2
                    response['message'] = "Url valida" 
                    response['data'] = data[0]

                    user_app = UserAppRepository().get(data[0].userapp_id)
                    response['user_name'] = user_app.user_name
                else:
                    response['message_code'] = 1
                    response['message'] = "La solicitud ha expirado"  

            return response 

        except ValidateAccount.DoesNotExist:
            return response

    def register_code_mobile(self,userapp_id):
        data = {}

        date_add = datetime.now()
        
        data['userapp_id'] = userapp_id
        data['code'] = str(random.randint(1000,9999))
        data['type_validate'] = "mobile"
        data['date_add'] = date_add
        data['date_expire'] =  date_add + timedelta(minutes = 5)

        validate_acc_sr = ValidateAccountSerializer(data = data)

        if validate_acc_sr.is_valid():
            validate_acc_sr.save()
        else:
            data['code'] = 0
    
        return data['code']

    def register_recovery_password(self,userapp_id,user_name,url_hash):
        data = {}
        date_add = datetime.now()
        
        data['userapp_id'] = userapp_id
        data['code'] = ""
        data['type_validate'] = "reset-password"
        data['date_add'] = date_add
        data['date_expire'] =  date_add + timedelta(minutes = 5)
        data['url_validate'] = url_hash

        validate_acc_sr = ValidateAccountSerializer(data = data)

        if validate_acc_sr.is_valid():
            validate_acc_sr.save()

from api.models import Order,OrderActivity
from api.repository.order_repository import OrderRepository
from api.repository.promotor_repository import PromotorRepository

class DashboardRepository():
    def stats(self,query):
        stats_orders = OrderRepository().get_stats(query)
        killers = PromotorRepository().total_promotors_by_operator(query)
        params = {"date_add":query['date_order']}
        params.update(query)
        new_killers = PromotorRepository().total_promotors_by_operator(params)
        
        response = {
            "deals":{
                "reservations":stats_orders['total_reserveds'],
                "pendients":stats_orders['total_pendients'],
                "canceleds":stats_orders['total_canceleds'],
                "payments":stats_orders['total_payments']
            },
            "killers":killers,
            "new_killers":new_killers
        }
        return response
from api.models import City

class CityRepository():
    def search(self,query):
        filters = {}
        
        if 'status' in query:
            filters['status'] = query['status']
        if 'name' in query:
            filters['name__icontains'] = query['name']
    
        return City.objects.filter(** filters)    
    
    def get(self,id):
        try:
            return City.objects.get(pk = id)
        except City.DoesNotExist:
            return None

    def delete(self,id):
        response = False
        city = self.get(id)
        
        if city != None:
            city.delete()
            response = True
        
        return response   

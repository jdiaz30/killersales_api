from twilio.rest import Client

class MobileMessageHelper():
    def __init__(self):
        account_sid = 'AC727e23f091f95905073ec6fb12bc3ba9'
        auth_token = '3c5fd401a13b5e01cd722fb28ac7651b'
        self.number_from = '+19796588437'
        self.client = Client(account_sid, auth_token)
    
    def send(self,message,number_phone):
        try:
            message = self.client.messages.create(
                              from_=self.number_from,
                              body=message,
                              to=number_phone
                          )
            return True
        except:
            return False

    def validate_phone(self,phone):
        try:
            phone_validate = self.client.lookups.phone_numbers(phone).fetch(type='carrier')
            return phone_validate.carrier
        except:
            return None
from django.core.mail import send_mail
from django.utils.html import strip_tags
from django.template.loader import render_to_string


class MailHelper():

    def send(self, data, template):
        template_text = self.get_template(template)
        html = render_to_string(template_text, data['data'])
        plain_message = strip_tags(html)

        send_mail(data['title'], plain_message, data['mail_from'],
                  data['mails_to'], html_message=html)

    def get_template(self, template):
        template_text = {
            "new-reservation": "email/new_reservation.html",
            "recovery-password": "email/recovery_password.html",
            "update-reservation": "email/reservation_confirm.html",
            "send_code_register": "email/send_code_validation.html"
        }
        return template_text[template]

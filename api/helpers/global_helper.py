import hashlib
import uuid


class GlobalHelper():

    def generated_hash(self, data):
        hashx = hashlib.sha224(data.encode())
        return hashx.hexdigest()

    def generated_code_internal_order(self, codeMax):
        return "BL-" + str(codeMax)

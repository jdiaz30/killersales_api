from django.http import HttpResponse, JsonResponse
from django.http import Http404
from django.db import IntegrityError


class ApiResponseHelper():
    def http_response(self, data, user_msg, status):
        data = {
            'data': data,
            'user_msg': user_msg
        }
        return JsonResponse(data, status=status)

from socketIO_client_nexus import SocketIO, LoggingNamespace


class SocketHelper():

    def __init__(self):
        self.socketIO = SocketIO(
            'https://localhost', 3000, LoggingNamespace, verify=False)

    """ def init(self):
        self.socketIO = SocketIO('https://localhost', 3000, LoggingNamespace) """

    def emit(self, event_name, data):
        self.socketIO.emit(event_name, data)

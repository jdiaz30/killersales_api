from rest_framework import pagination
from rest_framework.utils.urls import replace_query_param

class PaginatorHelper(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'

    def paginator_response(self, data):
        data = {
            'results': data,
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link()

        }
        return data

    def get_next_link(self):
        if not self.page.has_next():
            return None
        page_number = self.page.next_page_number()
        nexts = replace_query_param('', self.page_query_param, page_number)
        return nexts.replace("?page=", "")

    def get_previous_link(self):
        if not self.page.has_previous():
            return None
        page_number = self.page.previous_page_number()
        previous = replace_query_param('', self.page_query_param, page_number)
        return previous.replace("?page=", "")
from datetime import datetime,timedelta
import moment

class DateHelper():
    def diff_minutes(self,date_ini,date_end):
        dt1 = datetime.strptime(date_ini,'%Y-%m-%d %H:%M:%S')
        dt2 = datetime.strptime(date_end,'%Y-%m-%d %H:%M:%S')
        diff = dt2 - dt1
        minutes = (diff.seconds) / 60
        return minutes

    def date_format(self,date,format):
        #date_new = moment.date(date).timezone('America/Lima').format(format)
        date_new = moment.date(date).timezone('America/Lima').format(format)
        return date_new

    def date_week(self,date):
        date_obj = datetime.strptime(date,'%Y-%m-%d')
        day = datetime.strftime(date_obj,"%w")
        return int(day)

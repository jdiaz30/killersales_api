from calendar import timegm
from datetime import datetime

from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings

class JwtCustomizeHelper():
    def jwt_payload_handler(self, user):
        username_field = get_username_field()
        payload = {'user_id': user['id'], 'email': user['user_name'], 'username': user['user_name'],'type_user':user['type_user'],'is_admin':user['is_admin'],'related_id':user['related_id'],
                   'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA, username_field: user['user_name']}

        # Include original issued at time for a brand new token,
        # to allow token refresh
        if api_settings.JWT_ALLOW_REFRESH:
            payload['orig_iat'] = timegm(
                datetime.utcnow().utctimetuple()
            )

        if api_settings.JWT_AUDIENCE is not None:
            payload['aud'] = api_settings.JWT_AUDIENCE

        if api_settings.JWT_ISSUER is not None:
            payload['iss'] = api_settings.JWT_ISSUER

        return payload
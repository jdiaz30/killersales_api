# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-10-16 16:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='promotor',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='promotor_country', to='api.Country'),
        ),
    ]

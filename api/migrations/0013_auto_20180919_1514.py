# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-09-19 15:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_order_promotor'),
    ]

    operations = [
        migrations.AddField(
            model_name='operator',
            name='email_one',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='operator',
            name='email_three',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='operator',
            name='email_two',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='order',
            name='method_payment',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='total_payment',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=8),
        ),
    ]

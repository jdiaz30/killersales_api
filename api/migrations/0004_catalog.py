# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-08-14 16:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_promotor'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('status', models.IntegerField(default=0)),
                ('poster', sorl.thumbnail.fields.ImageField(upload_to='media')),
                ('operator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='operator', to='api.Operator')),
            ],
            options={
                'db_table': 'tb_catalog',
            },
        ),
    ]

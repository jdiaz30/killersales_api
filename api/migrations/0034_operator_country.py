# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-12-28 16:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0033_auto_20181210_2141'),
    ]

    operations = [
        migrations.AddField(
            model_name='operator',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='operator_country', to='api.Country'),
        ),
    ]

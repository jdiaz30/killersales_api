# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-08-11 02:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Operator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50)),
                ('web', models.CharField(blank=True, max_length=130)),
                ('phone', models.CharField(blank=True, max_length=12)),
                ('address', models.CharField(blank=True, max_length=130)),
                ('status', models.IntegerField()),
            ],
            options={
                'db_table': 'tb_operator',
            },
        ),
    ]

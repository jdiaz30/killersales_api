# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-11-22 19:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0030_order_date_order_upd'),
    ]

    operations = [
        migrations.AddField(
            model_name='promotor',
            name='type_promotor',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
        migrations.AlterField(
            model_name='userapp',
            name='last_login',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now, null=True),
        ),
    ]

from django.db import models
from django.contrib.auth.models import AbstractBaseUser,UserManager
from sorl.thumbnail import ImageField
from datetime import datetime

# Create your models here.

class Avatar(models.Model):
    name = models.CharField(max_length = 100,null = False,blank = False)
    status = models.IntegerField(blank = False,null = False,default = 1)
    url = ImageField(upload_to='avatars')
    is_free = models.BooleanField(default = False,null = False)
    kcoins = models.IntegerField(blank = False,null = False,default = 0)

    class Meta:
        db_table = 'tb_avatar'

    def __unicode__(self):
        return self.name

class UserApp(AbstractBaseUser):
    user_name = models.CharField(max_length = 100,unique = True)
    password = models.TextField(null = True, blank = True)
    status = models.IntegerField()
    last_login = models.DateTimeField(null = True,blank = True,default = datetime.now)
    type_user = models.CharField(null = True,blank = True,max_length = 80)#operator,promotor,cobrator
    is_admin = models.BooleanField(default = False,null = False)
    related_id = models.IntegerField(default = 0)

    USERNAME_FIELD = 'user_name'
    REQUIRED_FIELDS = ['status']

    objects = UserManager()

    class Meta:
        db_table = 'tb_userapp'
    
    def __unicode__(self):
        return self.user_name

class ValidateAccount(models.Model):
    code = models.CharField(max_length = 15,null = True,blank = True)
    date_add = models.DateTimeField(auto_now_add = True,blank = True,null= True)
    date_expire = models.DateTimeField(blank = True,null= True)
    type_validate = models.CharField(max_length = 20,null = False,blank = False)#mobile,email
    url_validate = models.TextField(null = False,blank = False)
    userapp = models.ForeignKey(UserApp,related_name = 'validate_account_userapp',blank = True,null = True)

    class Meta:
        db_table = 'tb_validate_account'
    
    def __unicode__(self):
        return self.code

class Dispositive(models.Model):
    so = models.CharField(max_length = 12,null = False,blank = True)
    uuid = models.TextField(null = True, blank = True)
    token = models.TextField(null = True, blank = True)
    status = models.IntegerField()
    userapp = models.ForeignKey(UserApp,related_name = 'UserAppDispositive',blank = True,null = True)

    class Meta:
        db_table = 'tb_dispositive'

    def __unicode__(self):
        return self.token

class City(models.Model):
    name = models.CharField(max_length = 50,null = False,blank = True)
    slug = models.CharField(max_length = 5,null = False,blank = True)
    status = status = models.IntegerField(null = False,default = 0)

    class Meta:
        db_table = 'tb_city'
    
    def __unicode__(self):
        return self.name

class Hotel(models.Model):
    name = models.CharField(max_length = 150,null = False,blank = True)
    status = status = models.IntegerField(null = False,default = 0)
    web = models.CharField(max_length = 130,null = False,blank = True)
    phone = models.CharField(max_length = 12,null = False,blank = True)
    address = models.CharField(max_length = 130,null = False,blank = True)

    class Meta:
        db_table = 'tb_hotel'

    def __unicode__(self):
        return self.name
    

class Clan(models.Model):
    name = models.CharField(max_length = 50,null = False,blank = True)
    poster = ImageField(upload_to='avatars')
    status = models.IntegerField(blank = False,null = False,default = 1)

    class Meta:
        db_table = 'tb_clan'
    
    def __unicode__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length = 100,null = False,blank = False)
    status = models.IntegerField(blank = False,null = False,default = 0)
    iso = models.CharField(max_length = 5,null = False,blank = False)
    number_area = models.CharField(max_length = 25,null = False,blank = False)

    class Meta:
        db_table = 'tb_country'
    
    def __unicode__(self):
        return self.name

class Money(models.Model):
    name = models.CharField(max_length = 20,null = False,blank = False)
    simbol = models.CharField(max_length = 5,null = False,blank = False)
    status = models.IntegerField(blank = False,null = False,default = 0)

    class Meta:
        db_table = 'tb_money'

    def __unicode__(self):
        return self.name    

class Operator(models.Model):
    name = models.CharField(max_length = 50,null = False,blank = True)
    web = models.CharField(max_length = 130,null = False,blank = True)
    phone = models.CharField(max_length = 12,null = False,blank = True)
    address = models.CharField(max_length = 130,null = False,blank = True)
    email_one = models.CharField(max_length = 60,null = False,blank = True)
    email_two = models.CharField(max_length = 60,null = False,blank = True)
    email_three = models.CharField(max_length = 60,null = False,blank = True)
    status = models.IntegerField(null = False)
    country = models.ForeignKey(Country,related_name = 'operator_country',blank = True,null = True)
    money = models.ForeignKey(Money,related_name = 'money_operator',blank = True,null = True)

    class Meta:
        db_table = 'tb_operator'

    def __unicode__(self):
        return self.name

class Divisa(models.Model):
    money_a = models.ForeignKey(Money,related_name = 'money_a',blank = True,null = True)
    money_b = models.ForeignKey(Money,related_name = 'money_b',blank = True,null = True)
    money_a_value = models.DecimalField(max_digits = 8,decimal_places = 2)
    money_b_value = models.DecimalField(max_digits = 8,decimal_places = 2)
    operator = models.ForeignKey(Operator,related_name = 'operator_divisa',blank = True,null = True)
    status = models.IntegerField(null = False,default = 1)

    class Meta:
        db_table = 'tb_divisa'

    def __unicode__(self):
        return self.money_a_value

class Promotor(models.Model):
    name = models.CharField(max_length = 50,null = False,blank = True)
    last_name = models.CharField(max_length = 80,null = False,blank = True)
    phone = models.CharField(max_length = 12,null = False,blank = True)
    address = models.CharField(max_length = 130,null = False,blank = True)
    email = models.CharField(max_length = 30,null = False,blank = True)
    status = models.IntegerField()
    avatar = models.ForeignKey(Avatar,related_name = 'avatar',blank = True,null = True,on_delete=models.SET_NULL)
    kcoins = models.IntegerField(blank = False,null = False,default = 0)
    clan = models.ForeignKey(Clan,related_name = 'clan',blank = True,null = True)
    operator = models.ForeignKey(Operator,related_name = 'operator_promotor',blank = True,null = True)
    is_freelancer = models.BooleanField(default = False,null = False)
    date_add = models.DateTimeField(auto_now_add = True,blank = True,null= True)
    country = models.ForeignKey(Country,related_name = 'promotor_country',blank = True,null = True)
    verify_email = models.BooleanField(default = False,null = False)
    verify_phone = models.BooleanField(default = False,null = False)
    type_promotor = models.CharField(max_length = 80,null = True,blank = True)#promotor,cobrator
    lat = models.FloatField(null = True,blank = True)
    lng = models.FloatField(null = True,blank = True)

    class Meta:
        db_table = 'tb_promotor'

    def __unicode__(self):
        return self.name

class Package(models.Model):
    date_add = models.DateTimeField(auto_now_add = True,blank = True,null= True)
    nro_act = models.IntegerField(blank = False,null = False,default = 0)
    dscto = models.IntegerField(blank = False,null = False,default = 0)
    status = models.IntegerField(blank = False,null = False,default = 0)
    operator = models.ForeignKey(Operator,related_name = 'operator_package',blank = True,null = True)

    class Meta:
        db_table = 'tb_package'

    def __unicode__(self):
        return self.dscto    

class Notify(models.Model):
    type_notify = models.CharField(max_length = 50,null = False,blank = False)#invite_clan,#update_order
    message = models.TextField(blank = True,null = False)
    action_id = models.IntegerField(null = False,blank = False)
    status = models.IntegerField(null = False,blank = False)#0 = no visto,1= aceptado,visto,2 = cancelado,
    to_promotor = models.ForeignKey(Promotor,related_name = 'promotor',blank = True,null = True)
    date_add = models.DateTimeField(auto_now_add = True,blank = True,null= True)

    class Meta:
        db_table = 'tb_notify'

    def __unicode__(self):
        return self.message

class Catalog(models.Model):
    name = models.CharField(max_length = 100,null = False,blank = False)
    operator = models.ForeignKey(Operator,related_name = 'operator')
    status = models.IntegerField(blank = False,null = False,default = 0)
    poster = ImageField(upload_to='media')

    class Meta:
        db_table = 'tb_catalog'

    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length = 100,null = False,blank = False)
    status = models.IntegerField(blank = False,null = False,default = 0)

    class Meta:
        db_table = 'tb_category'
    
    def __unicode__(self):
        return self.name

class Activity(models.Model):
    name = models.CharField(max_length = 100,null = False,blank = False)
    catalog = models.ForeignKey(Catalog,related_name = 'activities')
    status = models.IntegerField(blank = False,null = False,default = 0)
    price_pub_adult = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_pub_child = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_rep_adult = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_rep_child = models.DecimalField(max_digits = 8,decimal_places = 2)
    is_transport = models.BooleanField(default = False,null = False)
    city = models.ForeignKey(City,related_name = 'city_activity',null = True,blank = True)
    category = models.ForeignKey(Category,related_name = 'category',null = True,blank = True)
    poster = ImageField(upload_to='media',null = True,blank = True)
    max_capacity_day = models.IntegerField(blank = False,null = False,default = 0)
    description = models.TextField(null = True,blank = True)

    class Meta:
        db_table = 'tb_activity'
    
    def __unicode__(self):
        return self.name

class PickUp(models.Model):
    date_add = models.DateTimeField(auto_now_add = True,blank = False,null= False)
    lu_address = models.TextField(null = True,blank = True)
    ma_address = models.TextField(null = True,blank = True)
    mi_address = models.TextField(null = True,blank = True)
    ju_address = models.TextField(null = True,blank = True)
    vi_address = models.TextField(null = True,blank = True)
    sa_address = models.TextField(null = True,blank = True)
    do_address = models.TextField(null = True,blank = True)

    lu_time = models.CharField(max_length=10,null = True,blank = True)
    ma_time = models.CharField(max_length=10,null = True,blank = True)
    mi_time = models.CharField(max_length=10,null = True,blank = True)
    ju_time = models.CharField(max_length=10,null = True,blank = True)
    vi_time = models.CharField(max_length=10,null = True,blank = True)
    sa_time = models.CharField(max_length=10,null = True,blank = True)
    do_time = models.CharField(max_length=10,null = True,blank = True)

    activity = models.ForeignKey(Activity,related_name = 'activity_pickup',null = True,blank = True)

    class Meta:
        db_table = 'tb_pickup'
    
    def __unicode__(self):
        return self.lu_address

class Client(models.Model):
    name = models.CharField(max_length = 180,null = False,blank = False)
    email = models.CharField(max_length = 120,null = False,blank = False)
    phone = models.CharField(max_length = 20,null = False,blank = False)
    country = models.ForeignKey(Country,related_name = 'country',blank = True,null = True)

    class Meta:
        db_table = 'tb_client'
    
    def __unicode__(self):
        return self.name

class Order(models.Model):
    client =  models.ForeignKey(Client,related_name = 'Client',blank = True,null = True)
    code_internal = models.CharField(max_length = 100,blank = True,null = True)
    date_order = models.DateTimeField(auto_now_add = True,blank = True,null= True)
    date_order_upd = models.DateTimeField(blank = True,null = True,default = datetime.now)
    nro_hab = models.IntegerField(blank = False,null = False,default = 0)
    name_client = models.CharField(max_length = 120,null = False,blank = False)
    phone_client = models.CharField(max_length = 20,null = False,blank = True)
    email_client = models.CharField(max_length = 120,null = False,blank = True)
    nro_adults = models.IntegerField(blank = False,null = False,default = 0)
    nro_childs = models.IntegerField(blank = False,null = False,default = 0)
    nro_babys = models.IntegerField(blank = False,null = False,default = 0)
    operator = models.ForeignKey(Operator,related_name = 'operator_order',blank = True,null = True)
    city = models.ForeignKey(City,related_name = 'city',blank = True,null = True)
    hotel = models.ForeignKey(Hotel,related_name = 'hotel',blank = True,null = True)
    address_client = models.TextField(null = True,blank = True)
    total = models.DecimalField(max_digits = 8,decimal_places = 2)
    status = models.IntegerField(blank = False,null = False,default = 0)#0=pendiente,#1=reservado,#2=pagado,#3=cancelado 
    promotor = models.ForeignKey(Promotor,related_name = 'promotor_order',blank = True,null = True)
    method_payment = models.CharField(max_length = 80,null = True,blank = True)
    total_payment = models.DecimalField(max_digits = 8,decimal_places = 2,default =0)
    type_payment = models.CharField(max_length = 20,null = True,blank = True)
    money = models.ForeignKey(Money,related_name = 'money_order',blank = True,null = True)

    class Meta:
        db_table = 'tb_order'
    
    def __unicode__(self):
        return self.name_client

class OrderActivity(models.Model):
    order = models.ForeignKey(Order,related_name = 'order',blank = True,null = True)
    activity = models.ForeignKey(Activity,related_name = 'activities',blank = True,null = True)
    price_pub_adult = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_pub_child = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_rep_adult = models.DecimalField(max_digits = 8,decimal_places = 2)
    price_rep_child = models.DecimalField(max_digits = 8,decimal_places = 2)
    pick_up_date = models.DateTimeField(blank = True,null= True)
    pickup = models.ForeignKey(PickUp,related_name = 'pickup',blank = True,null = True)

    class Meta:
        db_table = 'tb_order_activity'
    
    def __unicode__(self):
        return self.pick_up_date

class KCoinsConfigure(models.Model):
    pax = models.IntegerField(blank = False,null = False,default = 0)
    type_pax = models.CharField(max_length = 20,null = True,blank = True)#adults,#childs
    kcoins = models.IntegerField(blank = False,null = False,default = 0)
    date_add = models.DateTimeField(blank = False,null= False,default = datetime.now)

    class Meta:
        db_table = 'tb_kcoins_configure'

    def __unicode__(self):
        return self.pax

class OrderCobrate(models.Model):
    order = models.ForeignKey(Order,related_name = 'order_cobrate',blank = True,null = True)
    date_add = models.DateTimeField(auto_now_add = True,blank = True,null= True)
    cobrator = models.ForeignKey(Promotor,related_name = 'cobrator',blank = True,null = True)
    status = models.IntegerField(blank = False,null = False,default = 0) #0=enviada,1 = pagado,2 = rechazado

    class Meta:
        db_table = 'tb_order_cobrate'

    def __unicode__(self):
        return self.status
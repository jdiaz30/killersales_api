from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import ClanSerializer

from api.repository.clan_repository import ClanRepository


class ClanView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = ClanRepository().search()
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = ClanSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de clanes",200)
    
    def post(self,request,format = None):
        serializer = ClanSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Clan registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Clan no registrado", 400)

class ClanDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = ClanRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Clan no encontrado", 400)
        else:
            clan_sr = ClanSerializer(data)
            return  ApiResponseHelper().http_response(clan_sr.data, "Clan encontrado", 200)

    def delete (self,request,id,format = None):
        response = ClanRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Clan eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Clan no eliminado", 400)

    def put (self,request,id,format = None):
        clan = ClanRepository().get(id)
        clan_sr = ClanSerializer(clan ,data = request.data,partial = True)

        if clan_sr.is_valid():
            clan_sr.save()
            return ApiResponseHelper().http_response(clan_sr.data, "Clan actualizado", 200)
        else:
            return ApiResponseHelper().http_response(clan_sr.errors, "Clan no actualizado", 400)
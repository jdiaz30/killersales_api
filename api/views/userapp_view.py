from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

import hashlib

from api.helpers.api_response_helper import ApiResponseHelper
from django.core.exceptions import FieldError

from api.helpers.paginator_helper import PaginatorHelper
from api.helpers.mail_helper import MailHelper
from api.helpers.global_helper import GlobalHelper

from api.repository.userapp_repository import UserAppRepository
from api.repository.dispositive_repository import DispositiveRepository
from api.repository.validate_account_repository import ValidateAccountRepository

from api.serializers import UserAppSerializer,DispositiveSerializer,ValidateAccount

class UserAppView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = UserAppRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = UserAppSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de usuarios",200)

    def post(self,request,format = None):
        serializer = UserAppSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Usuario registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Usuario no registrado", 400)

class UserAppAuthView(APIView):
    permission_classes = (AllowAny,)

    def post(self,request,format = None):
        data = UserAppRepository().auth(request.data,request.query_params)
        if data['message_code'] == 1:
            serializer = UserAppSerializer(data['data'])
            token = UserAppRepository().create_token_social(serializer.data)
            return ApiResponseHelper().http_response(token, data["message"], 200)
        else:
            return  ApiResponseHelper().http_response(data["data"], data["message"], 401)    
       

class UserAppDetailView(APIView):
    permission_classes =(IsAuthenticated,)

    def get(self,request,id,format = None):
        user = UserAppRepository().get(id)
        if user == None:
            return ApiResponseHelper().http_response("", "Usuario no encontrado", 400)
        else:
            user_sr = UserAppSerializer(user)
            return ApiResponseHelper().http_response(user_sr.data, "Usuario encontrado", 200)            

    def put(self,request,id,format = None):
        user_app = UserAppRepository().get(id)
        user_app_sr = UserAppSerializer(user_app ,data = request.data,partial = True)

        if user_app_sr.is_valid():
            user_app_sr.save()
            return ApiResponseHelper().http_response(user_app_sr.data, "Usuario actualizado", 200)
        else:
            return ApiResponseHelper().http_response(user_app_sr.data, "Usuario no actualizado", 400)
    
    def delete(self,request,id,format = None):
        user_delete = UserAppRepository().delete(id)

        if user_delete == True:
            return ApiResponseHelper().http_response("", "Usuario eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Usuario no eliminado", 400)
    
class UserAppDispositiveView(APIView):
    permission_classes =(AllowAny,)

    def post(self,request,id,format = None):
        serializer = DispositiveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Dispositive registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Dispositive no registrado", 400)
        

class UserAppRecoveryPasswordView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,user_name,format = None):
        user_data = UserAppRepository().get_by_user_name(user_name)

        if user_data != None:
            url_hash = GlobalHelper().generated_hash(user_data.user_name)
            url = "https://api.behlaaktours.xyz/reset-password/"+url_hash

            data_mail = {
                "data":{ "url":url},
                "mails_to":[user_name],
                "title":"Reestablecer password : Behlaaktours",
                "mail_from":"info@behlaaktours.com"
            }
            ValidateAccountRepository().register_recovery_password(user_data.id,user_data.user_name,url_hash)
            MailHelper().send(data_mail,"recovery-password")
            return ApiResponseHelper().http_response("", "Revise su bandeja de entrada", 200)
        else:
            return  ApiResponseHelper().http_response("", "Usuario no encontrado", 400)
    
    def put(self,request,user_name,format = None):
        user_app = UserAppRepository().get_by_user_name(user_name)

        if user_app != None:
            user_app_sr = UserAppSerializer(user_app ,data = request.data,partial = True)

            if user_app_sr.is_valid():
                user_app_sr.save()
                return ApiResponseHelper().http_response(user_app_sr.data, "Password actualizado", 200)
            else:
                return ApiResponseHelper().http_response(user_app_sr.data, "Password no actualizado", 400)
        else:
            return ApiResponseHelper().http_response("", "Usuario no existe", 400)
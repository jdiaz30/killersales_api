from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import CatalogSerializer,ActivitySerializer

from api.repository.catalog_repository import CatalogRepository
from api.repository.activity_repository import ActivityRepository

class CatalogView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = CatalogRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = CatalogSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de catálogos",200)
    
    def post(self,request,format = None):
        serializer = CatalogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Catálogo registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Catálogo no registrado", 400)

class CatalogDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = CatalogRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Catálogo no encontrado", 400)
        else:
            catalog_sr = CatalogSerializer(data)
            return  ApiResponseHelper().http_response(catalog_sr.data, "Catálogo encontrado", 200)

    def delete (self,request,id,format = None):
        response = CatalogRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Catálogo eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Catálogo no eliminado", 400)

    def put (self,request,id,format = None):
        catalog = CatalogRepository().get(id)
        catalog_sr = CatalogSerializer(catalog ,data = request.data,partial = True)

        if catalog_sr.is_valid():
            catalog_sr.save()
            return ApiResponseHelper().http_response(catalog_sr.data, "Catálogo actualizado", 200)
        else:
            return ApiResponseHelper().http_response(catalog_sr.errors, "Catálogo no actualizado", 400)

class CatalogActivityView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format=None):
        data = ActivityRepository().get_all_by_catalog(id)
        act_sr = ActivitySerializer(data,many = True)

        return ApiResponseHelper().http_response(act_sr.data, "Actividades del Catálogo", 200)
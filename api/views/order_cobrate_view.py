from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

import copy

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import OrderSerializer,OrderActivitySerializer,OrderCobrateSerializer

from api.repository.order_repository import OrderRepository
from api.repository.order_cobrate_repository import OrderCobrateRepository

from api.services.order_service import OrderService
from api.services.promotor_service import PromotorService


class OrderCobrateView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = OrderCobrateRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OrderCobrateSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de ordenes de compra",200)


class OrderCobrateDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self,request,id,format = None):
        order_cobrate = OrderRepository().get_order_cobrate(id)
        if order_cobrate == None:
            return  ApiResponseHelper().http_response("", "Orden cobranza no encontrado", 400)
        else:
            order_cobrate_sr = OrderCobrateSerializer(order_cobrate,data = request.data,partial = True)

            if order_cobrate_sr.is_valid():
                order_cobrate_sr.save()

                order = OrderRepository().get(order_cobrate.order_id)
                order_sr = OrderSerializer(order,data = {"status":2},partial = True)

                if order_sr.is_valid():
                    order_sr.save()

                    PromotorService().notify_cobrator_success(order_cobrate_sr.data['cobrator_id'],order.id,id)

                    total_kcoins = OrderService().get_total_kcoins(order.nro_childs,order.nro_adults)
                    if total_kcoins > 0:
                        PromotorService().add_kcoins(order.promotor_id,total_kcoins,order.id)
                return ApiResponseHelper().http_response(order_cobrate_sr.data, "Orden cobranza actualizado", 200)
            else:
                return ApiResponseHelper().http_response(order_cobrate_sr.data, "Orden cobranza no actualizado", 400)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import CategorySerializer

from api.repository.category_repository import CategoryRepository

class CategoryView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = CategoryRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = CategorySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de categorias",200)
    
    def post(self,request,format = None):
        serializer = CategorySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Categoria registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Categoria no registrado", 400)

class CategoryDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = CategoryRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Categoria no encontrado", 400)
        else:
            category_sr = CategorySerializer(data)
            return  ApiResponseHelper().http_response(category_sr.data, "Categoria encontrada", 200)


    def delete (self,request,id,format = None):
        response = CategoryRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Categoria eliminada", 200)
        else:
            return ApiResponseHelper().http_response("", "Categoria no eliminada", 400)

    def put (self,request,id,format = None):
        category = CategoryRepository().get(id)
        category_sr = CategorySerializer(category ,data = request.data,partial = True)

        if category_sr.is_valid():
            category_sr.save()
            return ApiResponseHelper().http_response(category_sr.data, "Categoria actualizado", 200)
        else:
            return ApiResponseHelper().http_response(category_sr.data, "Categoria no actualizado", 400)
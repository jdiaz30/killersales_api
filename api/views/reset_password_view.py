from django.shortcuts import render

from django.shortcuts import render_to_response
from api.repository.validate_account_repository import ValidateAccountRepository

def index(request,url):
	validate_url = ValidateAccountRepository().exists_url(url)
	return render_to_response("form/reset_password.html",{ "validate_url" : validate_url})

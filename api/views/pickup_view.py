from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import PickUpSerializer
from api.repository.pickup_repository import PickUpRepository

class PickUpView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = PickUpRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = PickUpSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de PickUps",200)
        

    def post(self,request,format = None):
        serializer = PickUpSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "PickUp registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "PickUp no registrado", 400)

class PickUpDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = PickUpRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "PickUp no encontrado", 400)
        else:
            pickup_sr = PickUpSerializer(data)
            return  ApiResponseHelper().http_response(pickup_sr.data, "PickUp encontrado", 200)


    def delete (self,request,id,format = None):
        response = PickUpRepository().delete(id)

        if response == True:
            return ApiResponseHelper().http_response("", "PickUp eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "PickUp no eliminado", 400)
    
    def put (self,request,id,format = None):
        pickup = PickUpRepository().get(id)
        pickup_sr = PickUpSerializer(pickup ,data = request.data,partial = True)

        if pickup_sr.is_valid():
            pickup_sr.save()
            return ApiResponseHelper().http_response(pickup_sr.data, "PickUp actualizado", 200)
        else:
            return ApiResponseHelper().http_response(pickup_sr.data, "PickUp no actualizado", 400)
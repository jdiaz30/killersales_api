from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import OperatorSerializer
from api.repository.operator_repository import OperatorRepository

class OperatorView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = OperatorRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OperatorSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de operadores",200)
        

    def post(self,request,format = None):
        serializer = OperatorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Operador registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Operador no registrado", 400)


class OperatorDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = OperatorRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Operador no encontrado", 400)
        else:
            operator_sr = OperatorSerializer(data)
            return  ApiResponseHelper().http_response(operator_sr.data, "Operador encontrado", 200)


    def delete (self,request,id,format = None):
        response = OperatorRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Operador eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Operador no eliminado", 400)
    
    def put (self,request,id,format = None):
        operator = OperatorRepository().get(id)
        operator_sr = OperatorSerializer(operator ,data = request.data,partial = True)

        if operator_sr.is_valid():
            operator_sr.save()
            return ApiResponseHelper().http_response(operator_sr.data, "Operador actualizado", 200)
        else:
            return ApiResponseHelper().http_response(operator_sr.data, "Operador no actualizado", 400)

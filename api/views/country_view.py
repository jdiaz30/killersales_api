from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import CountrySerializer

from api.repository.country_repository import CountryRepository

class CountryView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = CountryRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = CountrySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de paises",200)
    
    def post(self,request,format = None):
        serializer = CountrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Pais registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Pais no registrado", 400)

class CountryDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = CountryRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Pais no encontrado", 400)
        else:
            country_sr = CountrySerializer(data)
            return  ApiResponseHelper().http_response(country_sr.data, "Pais encontrado", 200)

    def delete (self,request,id,format = None):
        response = CountryRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Pais eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Pais no eliminado", 400)

    def put (self,request,id,format = None):
        country = CountryRepository().get(id)
        country_sr = CountrySerializer(country ,data = request.data,partial = True)

        if country_sr.is_valid():
            country_sr.save()
            return ApiResponseHelper().http_response(country_sr.data, "Pais actualizado", 200)
        else:
            return ApiResponseHelper().http_response(country_sr.errors, "Pais no actualizado", 400)
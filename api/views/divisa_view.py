from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import DivisaSerializer

from api.repository.divisa_repository import DivisaRepository

class DivisaView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = DivisaRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = DivisaSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de divisas",200)
    
    def post(self,request,format = None):
        serializer = DivisaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Divisa registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Divisa no registrado", 400)

class DivisaDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = DivisaRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Divisa no encontrado", 400)
        else:
            divisa_sr = DivisaSerializer(data)
            return  ApiResponseHelper().http_response(divisa_sr.data, "Divisa encontrado", 200)

    def delete (self,request,id,format = None):
        response = DivisaRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Divisa eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Divisa no eliminado", 400)

    def put (self,request,id,format = None):
        divisa = DivisaRepository().get(id)
        divisa_sr = DivisaSerializer(divisa ,data = request.data,partial = True)

        if divisa_sr.is_valid():
            divisa_sr.save()
            return ApiResponseHelper().http_response(divisa_sr.data, "Divisa actualizado", 200)
        else:
            return ApiResponseHelper().http_response("", divisa_sr.errors, 400)
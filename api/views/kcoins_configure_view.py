from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import KCoinsConfigureSerializer

from api.repository.kcoins_configure_repository import KCoinsConfigureRepository

class KCoinsConfigureView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = KCoinsConfigureRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = KCoinsConfigureSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de configuracion de KCoins",200)
    
    def post(self,request,format = None):
        serializer = KCoinsConfigureSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "KCoins registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "KCoins no registrado", 400)

class KcoinsConfigureDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = KCoinsConfigureRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "KCoins no encontrado", 400)
        else:
            kcoins_sr = KCoinsConfigureSerializer(data)
            return  ApiResponseHelper().http_response(kcoins_sr.data, "KCoins encontrado", 200)


    def delete (self,request,id,format = None):
        response = KCoinsConfigureRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "KCoins eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "KCoins no eliminado", 400)

    def put (self,request,id,format = None):
        kcoins = KCoinsConfigureRepository().get(id)
        kcoins_sr = KCoinsConfigureSerializer(kcoins ,data = request.data,partial = True)

        if kcoins_sr.is_valid():
            kcoins_sr.save()
            return ApiResponseHelper().http_response(kcoins_sr.data, "KCoins actualizado", 200)
        else:
            return ApiResponseHelper().http_response(kcoins_sr.data, "KCoins no actualizado", 400)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.repository.dashboard_repository import DashboardRepository
from api.helpers.api_response_helper import ApiResponseHelper

class DashboardView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        response = DashboardRepository().stats(request.query_params)
        return ApiResponseHelper().http_response(response,"Dashboard",200)
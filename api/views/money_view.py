from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import MoneySerializer

from api.repository.money_repository import MoneyRepository

class MoneyView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = MoneyRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = MoneySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de monedas",200)
    
    def post(self,request,format = None):
        serializer = MoneySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Moneda registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Moneda no registrado", 400)

class MoneyDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = MoneyRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Moneda no encontrado", 400)
        else:
            money_sr = MoneySerializer(data)
            return  ApiResponseHelper().http_response(money_sr.data, "Moneda encontrado", 200)

    def delete (self,request,id,format = None):
        response = MoneyRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Moneda eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Moneda no eliminado", 400)

    def put (self,request,id,format = None):
        money = MoneyRepository().get(id)
        money_sr = MoneySerializer(money ,data = request.data,partial = True)

        if money_sr.is_valid():
            money_sr.save()
            return ApiResponseHelper().http_response(money_sr.data, "Moneda actualizado", 200)
        else:
            return ApiResponseHelper().http_response(money_sr.data, "Moneda no actualizado", 400)
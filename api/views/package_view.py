from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.serializers import PackageSerializer

from api.repository.package_repository import PackageRepository

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

class PackageView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = PackageRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = PackageSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de Paquetes",200)

    def post(self,request,format = None):
        serializer = PackageSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Paquete registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Paquete no registrado", 400)

class PackageDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = PackageRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Paquete no encontrado", 400)
        else:
            package_sr = PackageSerializer(data)
            return  ApiResponseHelper().http_response(package_sr.data, "Paquete encontrado", 200)


    def delete (self,request,id,format = None):
        response = PackageRepository().delete(id)

        if response == True:
            return ApiResponseHelper().http_response("", "Paquete eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Paquete no eliminado", 400)

    
    def put (self,request,id,format = None):
        package = PackageRepository().get(id)
        package_sr = PackageSerializer(package ,data = request.data,partial = True)

        if package_sr.is_valid():
            package_sr.save()
            return ApiResponseHelper().http_response(package_sr.data, "Paquete actualizado", 200)
        else:
            return ApiResponseHelper().http_response(package_sr.data, "Paquete no actualizado", 400)
        
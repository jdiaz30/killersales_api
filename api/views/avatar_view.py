from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import AvatarSerializer

from api.repository.avatar_repository import AvatarRepository


class AvatarView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = AvatarRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = AvatarSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de avatars",200)
    
    def post(self,request,format = None):
        serializer = AvatarSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Avatar registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Avatar no registrado", 400)

class AvatarDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = AvatarRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Avatar no encontrado", 400)
        else:
            avatar_sr = AvatarSerializer(data)
            return  ApiResponseHelper().http_response(avatar_sr.data, "Avatar encontrado", 200)
    
    def delete (self,request,id,format = None):
        response = AvatarRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Avatar eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Avatar no eliminado", 400)
    
    def put (self,request,id,format = None):
        avatar = AvatarRepository().get(id)
        avatar_sr = AvatarSerializer(avatar ,data = request.data,partial = True)

        if avatar_sr.is_valid():
            avatar_sr.save()
            return ApiResponseHelper().http_response(avatar_sr.data, "Avatar actualizado", 200)
        else:
            return ApiResponseHelper().http_response(avatar_sr.data, "Avatar no actualizado", 400)




from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import HotelSerializer

from api.repository.hotel_repository import HotelRepository

class HotelView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = HotelRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = HotelSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de Hoteles",200)
    
    def post(self,request,format = None):
        serializer = HotelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Hoteles registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Hoteles no registrado", 400)


class HotelDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = HotelRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Hotel no encontrado", 400)
        else:
            hotel_sr = HotelSerializer(data)
            return  ApiResponseHelper().http_response(hotel_sr.data, "Hotel encontrado", 200)


    def delete (self,request,id,format = None):
        response = HotelRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Hotel eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Hotel no eliminado", 400)

    def put (self,request,id,format = None):
        hotel = HotelRepository().get(id)
        hotel_sr = HotelSerializer(hotel ,data = request.data,partial = True)

        if hotel_sr.is_valid():
            hotel_sr.save()
            return ApiResponseHelper().http_response(hotel_sr.data, "Hotel actualizado", 200)
        else:
            return ApiResponseHelper().http_response(hotel_sr.data, "Hotel no actualizado", 400)
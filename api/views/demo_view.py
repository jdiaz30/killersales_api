from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

import hashlib
import moment
from datetime import datetime,timedelta


from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper
from api.helpers.mobile_message_helper import MobileMessageHelper

from api.serializers import ValidateAccountSerializer

from api.repository.validate_account_repository import ValidateAccountRepository



class DemoView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
      
        d = MobileMessageHelper().validate_phone("+52984164726")
        if d != None:
            return ApiResponseHelper().http_response(d,"demos",200)
        else:
            return ApiResponseHelper().http_response("","Error",400)       
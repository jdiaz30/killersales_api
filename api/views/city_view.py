from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import CitySerializer

from api.repository.city_repository import CityRepository

class CityView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = CityRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = CitySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de ciudades",200)
    
    def post(self,request,format = None):
        serializer = CitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Ciudad registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Ciudad no registrado", 400)

class CityDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = CityRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Ciudad no encontrado", 400)
        else:
            city_sr = CitySerializer(data)
            return  ApiResponseHelper().http_response(city_sr.data, "Ciudad encontrado", 200)


    def delete (self,request,id,format = None):
        response = CityRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Ciudad eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Ciudad no eliminado", 400)

    def put (self,request,id,format = None):
        city = CityRepository().get(id)
        city_sr = CitySerializer(city ,data = request.data,partial = True)

        if city_sr.is_valid():
            city_sr.save()
            return ApiResponseHelper().http_response(city_sr.data, "Ciudad actualizado", 200)
        else:
            return ApiResponseHelper().http_response(city_sr.data, "Ciudad no actualizado", 400)

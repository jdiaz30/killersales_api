from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import PromotorSerializer,UserAppSerializer,OrderSerializer,OrderActivitySerializer

from api.repository.promotor_repository import PromotorRepository
from api.repository.order_repository import OrderRepository

from api.services.promotor_service import PromotorService

class PromotorView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = PromotorRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = PromotorSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de promotores",200)
        

    def post(self,request,format = None):
        serializer = PromotorSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Promotor registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Promotor no registrado", 400)

class PromotorDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        email = request.query_params['email'] if 'email' in request.query_params else None
        data = PromotorRepository().get(id) if email == None else PromotorRepository().get_by_email(email) 

        if data == None:
            return  ApiResponseHelper().http_response("", "Promotor no encontrado", 400)
        else:
            promotor_sr = PromotorSerializer(data)
            return  ApiResponseHelper().http_response(promotor_sr.data, "Promotor encontrado", 200)


    def delete (self,request,id,format = None):
        response = PromotorRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Promotor eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Promotor no eliminado", 400)
    
    def put (self,request,id,format = None):
        promotor = PromotorRepository().get(id)
        promotor_sr = PromotorSerializer(promotor ,data = request.data,partial = True)
        params_url = request.query_params

        if promotor_sr.is_valid():
            promotor_sr.save()
            if 'activate' in params_url:
                PromotorRepository().activate_account(promotor_sr.data,params_url['activate'])

            return ApiResponseHelper().http_response(promotor_sr.data, "Promotor actualizado", 200)
        else:
            return ApiResponseHelper().http_response(promotor_sr.data, "Promotor no actualizado", 400)

class PromotorOrdersView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = OrderRepository().get_promotor(id)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OrderSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de Ordenes de servicio",200)

class PromotorStatsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format=None):
        stats = PromotorService().get_stats(id)
        if stats == False:
            return ApiResponseHelper().http_response("","Stats Promotor",400)
        else:
            return ApiResponseHelper().http_response(stats,"Stats Promotor",200)

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

import copy
import csv
import xlwt

from django.http import HttpResponse

import braintree

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper
from api.helpers.socket_helper import SocketHelper
from api.helpers.mail_helper import MailHelper
from api.helpers.date_helper import DateHelper
from api.helpers.global_helper import GlobalHelper

from api.serializers import OrderSerializer,OrderActivitySerializer,OrderCobrateSerializer
from api.repository.order_repository import OrderRepository
from api.services.order_service import OrderService

class OrderView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = OrderRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OrderSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de ordenes",200)
    
    def post(self,request,format = None):
        order = request.data['order']
        country_client = request.data['country_client']
        order_activities = request.data['activities']
        
        total_childs = 0
        total_adults = 0

        client_data = { "name":order['name_client'],"email":order['email_client'],"phone":order['phone_client'],"country_id": country_client}
        client = OrderService().get_client(client_data)

        order['client_id'] =client['id']
        order_prev = copy.deepcopy(order)
        order_max_id = OrderRepository().get_last_id()
        order_prev['code_internal'] = GlobalHelper().generated_code_internal_order(order_max_id +1)

        serializer = OrderSerializer(data=order_prev)
        if serializer.is_valid():
            serializer.save()
            for activity in order_activities:
                activity['order_id'] = serializer.data['id']
                total_adults += float(activity['price_rep_adult']) if serializer.data['nro_adults'] > 0 else 0
                total_childs += float(activity['price_rep_child']) if serializer.data['nro_childs'] > 0 else 0

            order_act_sr = OrderActivitySerializer(data=order_activities,many=True)
            if order_act_sr.is_valid():
                order_act_sr.save()
            else:
                print("eeee",order_act_sr.errors)

            SocketHelper().emit('killer_sales:new-reservation',serializer.data)
            
            total_adults =  total_adults  if serializer.data['nro_adults'] == 0 else total_adults * int(serializer.data['nro_adults'])
            total_childs =  total_childs  if serializer.data['nro_childs'] == 0 else total_childs * int(serializer.data['nro_childs'])

            mails_to = ["joper30@gmail.com","casf1979@gmail.com"]

            if len(serializer.data['operator']['email_one'])>0:
                mails_to.append(serializer.data['operator']['email_one'])

            if len(serializer.data['operator']['email_two'])>0:
                mails_to.append(serializer.data['operator']['email_two'])

            if len(serializer.data['operator']['email_three'])>0:
                mails_to.append(serializer.data['operator']['email_three'])

            mails_list = ",".join(mails_to)

            order_data = copy.deepcopy(serializer.data)
            order_data['date_order_text']= DateHelper().date_format(serializer.data['date_order'],'YYYY-MM-DD HH:mm:ss')
           
            data_email = { 
                "data":{"order":order_data,"activities":order_act_sr.data,"total_childs":str(total_childs),"total_adults":str(total_adults)},
                "mails_to":mails_to,
                "title":"Nueva reservación : Operador :"+serializer.data['operator']['name'],
                "mail_from":"reservations@behlaaktours.com"
            }

            MailHelper().send(data_email,"new-reservation")
            
            return ApiResponseHelper().http_response(serializer.data, "Orden registrado", 200)
        else:
            print("Ordenb bi regio",serializer.errors)
            return  ApiResponseHelper().http_response(serializer.errors, "Orden no registrado", 400)

class OrderDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = OrderRepository().get(id)
        if data == None:
            return  ApiResponseHelper().http_response("", "Orden no encontrado", 400)
        else:
            activities = OrderRepository().get_activities(id)
            order_sr = OrderSerializer(data)
            order_act_sr = OrderActivitySerializer(activities,many=True)

            order_data = {
                "order":order_sr.data,
                "activities":  order_act_sr.data
            }
            return  ApiResponseHelper().http_response(order_data, "Orden encontrado", 200)
            
    def put(self,request,id, format = None):
        data = OrderRepository().get(id)
        if data == None:
            return  ApiResponseHelper().http_response("", "Orden no encontrado", 400)
        else:
            order = request.data['order']
            country_client = request.data['country_client']
            order_act = request.data['activities']
            order_act_delete = request.data['activities_deleted']
        
            total_childs = 0
            total_adults = 0

            client_data = { "name":order['name_client'],"email":order['email_client'],"phone":order['phone_client'],"country_id": country_client}
            client = OrderService().get_client(client_data)

            order['client_id'] =client['id']

            order_sr= OrderSerializer(data,data=order,partial = True)

            if order_sr.is_valid():
                order_sr.save()
                OrderService().delete_service_order(order_sr.data['id'],order_act_delete)
                OrderService().update_activities(order_sr.data['id'],order_act)

                return ApiResponseHelper().http_response("", "Orden actualizado", 200)
            else:
                print("d,",order_sr.errors,order['phone_client'])
                return ApiResponseHelper().http_response(order_sr.errors, "Orden no actualizado", 400)

class OrderDetailStatusView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,id,format = None):
        order = OrderRepository().get(id)
        paramas = request.query_params

        if order == None:
            return  ApiResponseHelper().http_response("", "Orden no encontrado", 400)
        else:
            order_sr = OrderSerializer(order ,data = request.data,partial = True)
            if order_sr.is_valid():
                order_sr.save()
                OrderService().update_order_status(order_sr.data)
                return  ApiResponseHelper().http_response("", "Orden actualizada", 200)
            else:
                return  ApiResponseHelper().http_response(order_sr.errors, "Orden no actualizada", 400)

class OrderActivityDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self,request,id,format = None):
        order_act = OrderRepository().get_activity(id,request.data['activity_id'])
        params = request.query_params

        if order_act == None:
            return ApiResponseHelper().http_response("","Actividad no actualizada",400)
        else:
            order_act_sr = OrderActivitySerializer(order_act,data = request.data,partial = True)
            if order_act_sr.is_valid():
                order_act_sr.save()
                if 'action' in params and params['action'] == 'pickup_update':
                    OrderService().update_pickup_notification(id)
                    
                return ApiResponseHelper().http_response(order_act_sr.data,"Actividad actualizada",200)
            else:
                return ApiResponseHelper().http_response(order_act_sr.errors,"Actividad no actualizada",400)

class OrderStatsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = OrderRepository().get_stats(request.query_params)
        
        return ApiResponseHelper().http_response(data,"Stats Ordenes",200)

class OrderSearchGeoView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = OrderRepository().search_geo(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OrderSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de ordenes",200)

class OrderCobrateView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,order_id,format = None):
        data = OrderRepository().search_order_cobrate(order_id)
        
        serializer = OrderCobrateSerializer(data)
        return ApiResponseHelper().http_response(serializer.data,"Orden de cobranza",200)

    def post(self,request,order_id,format = None):
        order = OrderRepository().get(order_id)
        if order == None:
            return  ApiResponseHelper().http_response("", "Orden no encontrado", 400)
        else:
            cobrator_id = request.data['cobrator_id']
            response = OrderService().generate_order_cobrate(order,cobrator_id)
            if response['success'] == True:
                return ApiResponseHelper().http_response("","Orden de cobranza enviada",200)
            else:
                return  ApiResponseHelper().http_response("", "Orden de cobranza no enviada", 400)

class OrderReportExcel(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,format = None):
        report = OrderService().report_by_asesor(request.query_params)
        #return ApiResponseHelper().http_response(report,"Lista de ordenes",200)
        return report

class OrderPaymentPaypal(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,format = None):
        gateway = braintree.BraintreeGateway(
            braintree.Configuration(
                environment = braintree.Environment.Sandbox,
                merchant_id = "p4cgwjtrz9m3gvg8",
                public_key = "2s7qfypyh5ztcf6c",
                private_key = "b4999e04275753b5d04699592141e3fd"
            )
        )

        client_token = gateway.client_token.generate()

        return ApiResponseHelper().http_response(client_token,"Token paypal",200)
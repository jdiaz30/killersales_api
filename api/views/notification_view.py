from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import NotifySerializer,PromotorSerializer

from api.repository.notification_repository import NotificationRepository
from api.repository.promotor_repository import PromotorRepository

class NotificationView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = NotificationRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = NotifySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de Notificaciones",200)

    def post(self,request,format = None):
        serializer = NotifySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Notificación registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Notificación no registrado", 400)

class NotificationDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = NotificationRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Notificación no encontrado", 400)
        else:
            notify_sr = NotifySerializer(data)
            return  ApiResponseHelper().http_response(notify_sr.data, "Notificación encontrado", 200)

    def delete (self,request,id,format = None):
        response = NotificationRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Notificación eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Notificación no eliminado", 400)

    def put (self,request,id,format = None):
        notify = NotificationRepository().get(id)
        notify_sr = NotifySerializer(notify ,data = request.data,partial = True)

        params = request.query_params

        if notify_sr.is_valid():
            notify_sr.save()
            if request.data['type_notify'] == "invite_clan" and request.data['status'] == 1 :
                promotor = PromotorRepository().get(request.data['to_promotor_id'])
                promotor.clan_id = request.data['action_id']
                promotor.save()
            return ApiResponseHelper().http_response(notify_sr.data, "Notificación actualizado", 200)
        else:
            return ApiResponseHelper().http_response(notify_sr.errors, "Notificación no actualizado", 400)
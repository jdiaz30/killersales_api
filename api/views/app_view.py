from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

import random

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper
from api.helpers.mobile_message_helper import MobileMessageHelper
from api.helpers.mail_helper import MailHelper

from api.serializers import CountrySerializer,PromotorSerializer,UserAppSerializer,ValidateAccountSerializer,CatalogSerializer,OperatorSerializer

from api.repository.country_repository import CountryRepository
from api.repository.validate_account_repository import ValidateAccountRepository
from api.repository.promotor_repository import PromotorRepository
from api.repository.operator_repository import OperatorRepository

class AppCountryView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,format = None):
        data = CountryRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = CountrySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de paises",200)

class AppOperatorView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,format = None):
        data = OperatorRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = OperatorSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de operadores",200)


class AppPromotorView(APIView):
    permission_classes = (AllowAny,)

    def post(self,request,format = None):
        promotor = request.data['promotor']
        country = request.data['country']
        promotor_sr = PromotorSerializer(data=promotor)

        if promotor_sr.is_valid():
            promotor_sr.save()
            phone = country['number_area'] + promotor['phone']
            
        
            user_data = request.data['user']
            user_data['related_id'] = promotor_sr.data['id']
            userapp_sr = UserAppSerializer(data = user_data)

            if userapp_sr.is_valid():
                userapp_sr.save()
                phone = promotor_sr.data['country']['number_area'] + promotor_sr.data['phone']
                    
                code_activate = ValidateAccountRepository().register_code_mobile(userapp_sr.data['id'])
                if code_activate != 0:
                    data_email = { 
                        "data": {"code" : code_activate},
                        "mails_to":[promotor_sr.data['email'],"joper30@gmail.com"],
                        "title":"Código de activación : BehlaakTours",
                        "mail_from":"noresponder@behlaaktours.com"
                    }

                    MailHelper().send(data_email,"send_code_register")
                    #MobileMessageHelper().send("El codigo de activacion de KillerSales es :"+str(code_activate),phone)
                    
                return ApiResponseHelper().http_response(userapp_sr.data, "Promotor registrado", 200)
            else:
                return  ApiResponseHelper().http_response(userapp_sr.errors, "Promotor no registrado", 400)
            
        else:
            print("erors",promotor_sr.errors)
            return  ApiResponseHelper().http_response(promotor_sr.errors, "El correo ingresado ya esta registrado", 400)

class AppVerifyCodeView(APIView):
    permission_classes = (AllowAny,)

    def post(self,request,format = None):
        data = ValidateAccountRepository().exists_code(request.data)
        if data['message_code'] != 0:
            if data['data'] == None:
                return  ApiResponseHelper().http_response(data['message_code'],data['message'], 400)
            else:
                validate_sr = ValidateAccountSerializer(data['data'])
                PromotorRepository().validate_phone(validate_sr.data['userapp_id'])
                return  ApiResponseHelper().http_response(validate_sr.data,data['message'], 200)
        else:
            return  ApiResponseHelper().http_response(data['message_code'], "Codigo no encontrado", 400)

class AppVerifyCodeSendView(APIView):
    permission_classes = (AllowAny,)

    def post(self,request,format = None):
        userapp = request.data['userapp']
        promotor = request.data['promotor']
        try:
            code_activate = ValidateAccountRepository().register_code_mobile(userapp['id'])
            if code_activate != 0:
                data_email = { 
                    "data": {"code" : code_activate},
                    "mails_to":[userapp['user_name'],"joper30@gmail.com"],
                    "title":"Código de activación : BehlaakTours",
                    "mail_from":"noresponder@behlaaktours.com"
                }

                MailHelper().send(data_email,"send_code_register")
                return  ApiResponseHelper().http_response("", "Codigo enviado", 200)
            else:
                return  ApiResponseHelper().http_response("", "No se pudo enviar el codigo", 400)
        except:
            return  ApiResponseHelper().http_response("", "No se pudo enviar el codigo", 400)


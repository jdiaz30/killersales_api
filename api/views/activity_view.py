from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes
from rest_framework.parsers import MultiPartParser,JSONParser

import copy

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper

from api.serializers import ActivitySerializer,PickUpSerializer

from api.repository.activity_repository import ActivityRepository
from api.repository.pickup_repository import PickUpRepository
from api.repository.order_repository import OrderRepository

class ActivityView(APIView):
    permission_classes = (IsAuthenticated,)
 
    def get(self,request,format = None):
        data = ActivityRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = ActivitySerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de actividades",200)
    
    def post(self,request,format = None):
        option = request.query_params['option']
        if option == "create":
            serializer = ActivitySerializer(data=request.data,many = True)
            if serializer.is_valid():
                serializer.save()
                return ApiResponseHelper().http_response(serializer.data, "Actividad registrado", 200)
            else :
                return ApiResponseHelper().http_response(serializer.errors, "Actividad no registrado", 400)
        else:
            activity = request.data
            if 'id' in activity:
                if 'deleted' in activity:
                    ActivityRepository().delete(activity['id'])
                else:
                    activity_item = ActivityRepository().get(activity['id'])
                    act_sr = ActivitySerializer(activity_item,data = activity,partial = True)
                    if act_sr.is_valid():
                        act_sr.save()      
            else:
                act_sr = ActivitySerializer(data = activity)
                if act_sr.is_valid():
                    act_sr.save()            
                
            return ApiResponseHelper().http_response("", "Actividad registrados", 200)

class ActivityPickupView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id, format = None):
        try:
            data = PickUpRepository().get_all_by_activity(id)
            paginator = PaginatorHelper()
            result_page = paginator.paginate_queryset(data,request)

            act_pick_sr = PickUpSerializer(result_page,many = True)

            return ApiResponseHelper().http_response(paginator.paginator_response(act_pick_sr.data),"Listado de Pickups",200)
        except:
            return ApiResponseHelper().http_response("","Ocurrio un error inesperado",500)

class  ActivityPickupCloneView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,id,format = None):
        clone_activity = request.data['activity_id']
        pickups = PickUpRepository().search({"activity_id":clone_activity})
        pickups_clone = []

        for pickup in pickups:
            sr_pickup = PickUpSerializer(pickup)
            clone_pickup = copy.deepcopy(sr_pickup.data)
            clone_pickup['activity_id'] = id
        
            pickups_clone.append(clone_pickup)

        sr_pickups_clone = PickUpSerializer(data = pickups_clone, many = True)

        if sr_pickups_clone.is_valid():
            sr_pickups_clone.save()
            return ApiResponseHelper().http_response("","Pickup clonado",200)
        else:
            return ApiResponseHelper().http_response(sr_pickups_clone.errors,"Pickup no se pudo clonar",400)


class ActivityCatalogCloneView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format = None):
        catalog_id = request.data['catalog_id']
        activities_id = request.data['activities_id']

        activities_clone = []

        try:

            for activity in activities_id:
                activity_data = ActivityRepository().get(activity)
                sr_activity = ActivitySerializer(activity_data)

                pickups_activity = PickUpRepository().get_all_by_activity(activity)
                
                clone_activity = copy.deepcopy(sr_activity.data)
                clone_activity['catalog_id'] = catalog_id
                clone_activity['poster'] = activity_data.poster

                #del clone_activity['poster'] 
            
                sr_activity_clone = ActivitySerializer(data = clone_activity, many = False)

                if sr_activity_clone.is_valid():
                    sr_activity_clone.save()

                    pickups_activity_clone = []

                    for pickup in pickups_activity:
                        sr_pickup_activity = PickUpSerializer(pickup)

                        clone_pickup_activity = copy.deepcopy(sr_pickup_activity.data)
                        clone_pickup_activity['activity_id'] = sr_activity_clone.data['id']

                        pickups_activity_clone.append(clone_pickup_activity)

                    sr_pickups_activity_clone = PickUpSerializer(data = pickups_activity_clone, many = True)

                    if sr_pickups_activity_clone.is_valid():
                        sr_pickups_activity_clone.save()

                else:
                    print("dsdsdLLLLL",sr_activity_clone.errors)

            return ApiResponseHelper().http_response("","Actividades clonadas",200)
        except:
            return ApiResponseHelper().http_response("","Actividades no se pudo clonar",400)


class ActivityAvailabilityView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        try:
            date = request.query_params['date']
            data = ActivityRepository().get_available_by_date(id,date)
            return ApiResponseHelper().http_response(data,"Disponibilidad",200) 
        except:
            return ApiResponseHelper().http_response("","Ocurrio un error interno",500)           

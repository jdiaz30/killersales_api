from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import permission_classes

from api.helpers.api_response_helper import ApiResponseHelper
from api.helpers.paginator_helper import PaginatorHelper
from api.helpers.mobile_message_helper import MobileMessageHelper

from api.serializers import ClientSerializer

from api.repository.client_repository import ClientRepository

class ClientView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,format = None):
        data = ClientRepository().search(request.query_params)
        paginator = PaginatorHelper()
        result_page = paginator.paginate_queryset(data,request)

        serializer = ClientSerializer(result_page,many = True)
        return ApiResponseHelper().http_response(paginator.paginator_response(serializer.data),"Lista de clientes",200)

    def post(self,request,format = None):
        serializer = ClientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return ApiResponseHelper().http_response(serializer.data, "Cliente registrado", 200)
        else:
            return  ApiResponseHelper().http_response(serializer.errors, "Cliente no registrado", 400)

class ClientDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,id,format = None):
        data = ClientRepository().get(id)

        if data == None:
            return  ApiResponseHelper().http_response("", "Cliente no encontrado", 400)
        else:
            client_sr = ClientSerializer(data)
            return  ApiResponseHelper().http_response(client_sr.data, "Cliente encontrado", 200)

    def put (self,request,id,format = None):
        client = ClientRepository().get(id)
        client_sr = ClientSerializer(client ,data = request.data,partial = True)

        if client_sr.is_valid():
            client_sr.save()
            return ApiResponseHelper().http_response(client_sr.data, "Cliente actualizado", 200)
        else:
            return ApiResponseHelper().http_response(client_sr.data, "Cliente no actualizado", 400)

    def delete (self,request,id,format = None):
        response = ClientRepository().delete(id)
        if response == True:
            return ApiResponseHelper().http_response("", "Cliente eliminado", 200)
        else:
            return ApiResponseHelper().http_response("", "Cliente no eliminado", 400)

class ClientVerifyPhoneView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format = None):
        phone = request.data['phone']
        validate = MobileMessageHelper().validate_phone(phone)
        if validate != None:
            return ApiResponseHelper().http_response("", "Teléfono es correcto", 200)
        else:
            return ApiResponseHelper().http_response("", "Teléfono no es válido", 400)

from django.conf.urls import url,include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from api.views import userapp_view,operator_view,promotor_view,catalog_view,activity_view,avatar_view,clan_view,notification_view,city_view,hotel_view,order_view,dashboard_view,country_view,app_view,package_view,category_view,pickup_view,demo_view,kcoins_configure_view,client_view,money_view,divisa_view,order_cobrate_view

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^',include(router.urls)),
    #url(r'^auth/', obtain_jwt_token),
    url(r'^auth/$',userapp_view.UserAppAuthView.as_view()),
    url(r'^users/$',userapp_view.UserAppView.as_view()),
    url(r'^users/(?P<id>[0-9]+)/$',userapp_view.UserAppDetailView.as_view()),
    url(r'^users/(?P<id>[0-9]+)/dispositives/$',userapp_view.UserAppDispositiveView.as_view()),
    url(r'^users/reset-password/(?P<user_name>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$',userapp_view.UserAppRecoveryPasswordView.as_view()),

    url(r'^moneys/$',money_view.MoneyView.as_view()),
    url(r'^moneys/(?P<id>[0-9]+)/$',money_view.MoneyDetailView.as_view()),

    url(r'^divisas/$',divisa_view.DivisaView.as_view()),
    url(r'^divisas/(?P<id>[0-9]+)/$',divisa_view.DivisaDetailView.as_view()),

    url(r'^orders/$',order_view.OrderView.as_view()),
    url(r'^orders/(?P<id>[0-9]+)/$',order_view.OrderDetailView.as_view()),
    url(r'^orders/stats/$',order_view.OrderStatsView.as_view()),
    url(r'^orders/(?P<id>[0-9]+)/status/$',order_view.OrderDetailStatusView.as_view()),
    url(r'^orders/(?P<id>[0-9]+)/activities/$',order_view.OrderActivityDetailView.as_view()),
    url(r'^orders/(?P<order_id>[0-9]+)/cobrate/$',order_view.OrderCobrateView.as_view()),
    url(r'^orders/search-geo/$',order_view.OrderSearchGeoView.as_view()),
    url(r'^orders/report/excel/$',order_view.OrderReportExcel.as_view()),
    url(r'^orders/paypal/$',order_view.OrderPaymentPaypal.as_view()),

    url(r'^orders-cobrate/$',order_cobrate_view.OrderCobrateView.as_view()),
    url(r'^orders-cobrate/(?P<id>[0-9]+)/$',order_cobrate_view.OrderCobrateDetailView.as_view()),

    url(r'^hotels/$',hotel_view.HotelView.as_view()),
    url(r'^hotels/(?P<id>[0-9]+)/$',hotel_view.HotelDetailView.as_view()),

    url(r'^cities/$',city_view.CityView.as_view()),
    url(r'^cities/(?P<id>[0-9]+)/$',city_view.CityDetailView.as_view()),

    url(r'^clans/$',clan_view.ClanView.as_view()),
    url(r'^clans/(?P<id>[0-9]+)/$',clan_view.ClanDetailView.as_view()),

    url(r'^notifications/$',notification_view.NotificationView.as_view()),
    url(r'^notifications/(?P<id>[0-9]+)/$',notification_view.NotificationDetailView.as_view()),

    url(r'^avatars/$',avatar_view.AvatarView.as_view()),
    url(r'^avatars/(?P<id>[0-9]+)/$',avatar_view.AvatarDetailView.as_view()),

    url(r'^operators/$',operator_view.OperatorView.as_view()),
    url(r'^operators/(?P<id>[0-9]+)/$',operator_view.OperatorDetailView.as_view()),

    url(r'^clients/$',client_view.ClientView.as_view()),
    url(r'^clients/(?P<id>[0-9]+)/$',client_view.ClientDetailView.as_view()),
    url(r'^clients/verify-phone/$',client_view.ClientVerifyPhoneView.as_view()),

    url(r'^promotors/$',promotor_view.PromotorView.as_view()),
    url(r'^promotors/(?P<id>[0-9]+)/$',promotor_view.PromotorDetailView.as_view()),
    url(r'^promotors/(?P<id>[0-9]+)/stats/$',promotor_view.PromotorStatsView.as_view()),
    url(r'^promotors/(?P<id>[0-9]+)/orders/$',promotor_view.PromotorOrdersView.as_view()),

    url(r'^catalogs/$',catalog_view.CatalogView.as_view()),
    url(r'^catalogs/(?P<id>[0-9]+)/$',catalog_view.CatalogDetailView.as_view()),
    url(r'^catalogs/(?P<id>[0-9]+)/activities/$',catalog_view.CatalogActivityView.as_view()),

    url(r'^activities/$',activity_view.ActivityView.as_view()),
    url(r'^activities/(?P<id>[0-9]+)/pickups/$',activity_view.ActivityPickupView.as_view()),
    url(r'^activities/(?P<id>[0-9]+)/pickups/clone/$',activity_view.ActivityPickupCloneView.as_view()),
    url(r'^activities/(?P<id>[0-9]+)/availabilities/$',activity_view.ActivityAvailabilityView.as_view()),
    url(r'^activities/clone/$',activity_view.ActivityCatalogCloneView.as_view()),

    url(r'^countries/$',country_view.CountryView.as_view()),
    url(r'^countries/(?P<id>[0-9]+)/$',country_view.CountryDetailView.as_view()),

    url(r'^categories/$',category_view.CategoryView.as_view()),
    url(r'^categories/(?P<id>[0-9]+)/$',category_view.CategoryDetailView.as_view()),

    url(r'^packages/$',package_view.PackageView.as_view()),
    url(r'^packages/(?P<id>[0-9]+)/$',package_view.PackageDetailView.as_view()),

    url(r'^pickups/$',pickup_view.PickUpView.as_view()),
    url(r'^pickups/(?P<id>[0-9]+)/$',pickup_view.PickUpDetailView.as_view()),

    url(r'^kcoins/$',kcoins_configure_view.KCoinsConfigureView.as_view()),
    url(r'^kcoins/(?P<id>[0-9]+)/$',kcoins_configure_view.KcoinsConfigureDetailView.as_view()),

    url(r'^demo/$',demo_view.DemoView.as_view()),

    url(r'^app/countries/$',app_view.AppCountryView.as_view()),
    url(r'^app/operators/$',app_view.AppOperatorView.as_view()),
    url(r'^app/promotors/$',app_view.AppPromotorView.as_view()),
    url(r'^app/verify-code/$',app_view.AppVerifyCodeView.as_view()),
    url(r'^app/send-code/$',app_view.AppVerifyCodeSendView.as_view()),

    url(r'^dashboard/$',dashboard_view.DashboardView.as_view()),
]